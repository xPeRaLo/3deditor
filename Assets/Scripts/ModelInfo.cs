﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using Unity.Mathematics;
using UnityEngine;



public enum StorageType { Server, Forge }

[Serializable] 
public class ModelInfo
{


	public int id;
	public string name;
	public string level;
	public string order;
	public string idDad;
	public string scene;

	public string modelName;
	public string modelURL;
	public string thumbnailURL;

	public StorageType storageType;

	public float3? position;
	public float3? rotation;
	public float3? scale;

	public bool set = false;
	public bool hasModel = true;

	public List<KPIProperty> kpis;
	public List<ModelInfo> children;

	/// <summary>
	/// Modelos descargados desde el servidor
	/// </summary>
	public ModelInfo(string name, string level, string order, string idDad,string modelName,string modelURL, int id, string thumbnailURL = null)
	{
		this.name = name;
		this.level = level;
		this.order = order;
		this.idDad = idDad;

		this.modelName = modelName;
		this.modelURL = modelURL;
		this.thumbnailURL = thumbnailURL;
		this.id = id;

		storageType = StorageType.Server;

		if (modelURL == null || modelURL == "")
			hasModel = false;
	}

	/// <summary>
	/// Modelos descargados desde forge
	/// </summary>
	public ModelInfo(string name, string level, string order, string idDad, string modelName, string forgeScene, string forgeURN, int id, string thumbnailURL = null)
	{
		this.name = name;
		this.level = level;
		this.order = order;
		this.idDad = idDad;

		this.modelName = modelName;
		modelURL = forgeURN;
		scene = forgeScene;
		this.thumbnailURL = thumbnailURL;
		this.id = id;

		storageType = StorageType.Forge;

		if (modelURL == null || modelURL == "")
			hasModel = false;
	}


	public void Set(float3 pos, float3 rot, float3 s)
	{
		position = new float3(pos);
		rotation = new float3(rot);
		scale = new float3(s);
		set = true;
	}

	/// <summary>
	/// 
	/// </summary>
	/// <returns> True if model has children </returns>
	public bool hasChildren()
	{
		if (children != null && children.Count > 0)
			return true;

		return false;
	}

	/// <summary>
	/// 
	/// </summary>
	/// <returns> True if the model has KPIs </returns>
	public bool hasKPI()
	{
		if (kpis != null && kpis.Count > 0)
			return true;

		return false;
	}


}
