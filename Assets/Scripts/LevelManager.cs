﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils.Singleton;

public class LevelManager : Singleton<LevelManager>
{

	FreeCameraMovement freeCam;
	List<IInteractableObject> interactables;

	#region Properties

	int currentLevel = 1;
	public int CurrentLevel
	{
		get { return currentLevel; }
		set
		{
			previousLevel = currentLevel;

			currentLevel = value;

			StartCoroutine(VisualizeLevel());
		}
	}

	int maxLevel = 1;

	public int MaxLevel
	{
		get { return maxLevel; }
		private set { maxLevel = value; }
	}

	int previousLevel = 1;

	public int PreviousLevel
	{
		get { return previousLevel; }
	}

	#endregion

	private void Awake()
	{
		freeCam = FindObjectOfType<FreeCameraMovement>();
	}

	private void Start()
	{
		#region Eventos botones

		UIReferences.Instance.FPSButton.onClick.AddListener(() =>
		{
			freeCam.enabled = true; // Activamos movimiento libre


			UIReferences.Instance.FPSButton.gameObject.SetActive(false);
			UIReferences.Instance.QuitFPSButton.gameObject.SetActive(true);
		});

		UIReferences.Instance.QuitFPSButton.onClick.AddListener(() =>
		{
			UIReferences.Instance.FPSButton.interactable = false;

			UIReferences.Instance.FPSButton.gameObject.SetActive(true);
			UIReferences.Instance.QuitFPSButton.gameObject.SetActive(false);

			freeCam.enabled = false; // Desactivamos movimiento libre
		});

		#endregion
	}

	IEnumerator  VisualizeLevel()
	{
		yield return new WaitUntil(() => !FileImporter.Instance.isDownloading);

		if (interactables == null)
			yield break;

		foreach (IInteractableObject interactable in interactables)
		{
			if (int.Parse(interactable.info.level) <= CurrentLevel)
				interactable.Active(true);
			else
				interactable.Active(false);
		}
	}

	public void AddInteractable(IInteractableObject obj)
	{
		if (interactables == null)
			interactables = new List<IInteractableObject>();

		interactables.Add(obj);
	}

	public void SetMaxLevel()
	{
		foreach (ModelInfo info in FileImporter.Instance.models)
		{
			if (int.Parse(info.level) > MaxLevel)
			{
				MaxLevel = int.Parse(info.level);
			}
		}
	}	
	public void ClearInteractables()
	{
		if (interactables != null)
			interactables.Clear();
	}
}
