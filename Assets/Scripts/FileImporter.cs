﻿
using Autodesk.Forge.ARKit;
using Michsky.UI.ModernUIPack;
using System;
using System.Collections;
using System.Collections.Generic;
using TriLibCore;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Utils.FileSystem;
using static Michsky.UI.ModernUIPack.CustomDropdown;
using Utils.Singleton;
using UnityEditor;

public class FileImporter : Singleton<FileImporter>
{
	[HideInInspector] public List<ModelInfo> models = new List<ModelInfo>();
	[HideInInspector] public List<KPIProperty> kpiList = new List<KPIProperty>();
	List<ModelInfo> nestedModel = new List<ModelInfo>();

	[Header("Scriptable Objects")]
	public Prefabs prefabs;
	public Graphs graphs;
	public UserData userData;
	public ExportedModelsSO exportedModels;

	private GameObject _spawnedObject;
	public GameObject SpawnedObject
	{
		get { return _spawnedObject; }
		set { _spawnedObject = value; }
	}
	
	Vector3 currentPosition = new Vector3(0, 0, 0);
	Vector3 currentRotation = new Vector3(0, 0, 0);
	Vector3 currentScale = new Vector3(1, 1, 1);

	[HideInInspector] public bool isDownloading = false;

	List<GameObject> currentObjectsInScene = new List<GameObject>();

	int notificationCount = 0;

	Image fill;

	ModelInfo currentModel = null;

	public List<ModelData> modeldatas;
	public float LoadProgress { get; set; }

	private void Awake()
	{
		fill = UIReferences.Instance.loadingBar.transform.Find("fill").GetComponent<Image>();
	}

	/// <summary>
	/// Loads the models stored on the device.
	/// </summary>
	public void LoadStoredModels()
	{
		if (exportedModels.models.Count > 0)
		{
			foreach (var i in exportedModels.models)
			{
				if (i.info.set && i.pf != null)
				{
					GameObject instance = Instantiate(i.pf);
					instance.transform.position = new Vector3(i.position.x, i.position.y, i.position.z);
					instance.transform.rotation = Quaternion.Euler(i.rotation.x, i.rotation.y, i.rotation.z);
					instance.transform.localScale = new Vector3(i.scale.x, i.scale.y, i.scale.z);
					instance.name = i.info.modelName;

					RecursiveColliderAdd(instance.transform, i.info); // Añadimos colliders a cada malla que conforma esa geometria

					instance.transform.SetParent(UIReferences.Instance.objectContainer);
				}
			}
		}
	}

	public void LoadData()
	{
		LoadKPI();
	}

	#region Model Importing

	/// <summary>
	/// Instantiates the model stored locally or in the server
	/// </summary>
	/// <param name="url">URL where the model is stored</param>
	/// <param name="format">Format of the model (only FBX, OBJ, GLTF2, STL, PLY, 3MF, and ZIP are supported)</param>
	public void SpawnProp(string url, string format = "fbx")
	{
		if (isDownloading || url == "" || url == null)
			return;

		isDownloading = true;

		SpawnedObject.transform.parent = UIReferences.Instance.objectContainer;

		var assetLoaderOptions = AssetLoader.CreateDefaultLoaderOptions(true);
		var webRequest = AssetDownloader.CreateWebRequest(url);
		AssetDownloader.LoadModelFromUri(webRequest, OnLoad, OnMaterialsLoad, OnProgress, OnError, SpawnedObject, assetLoaderOptions, null, format, false);
		UIReferences.Instance.loadingBar.gameObject.SetActive(true);
		SpawnedObject.layer = LayerMask.NameToLayer("WorldGeometry");
	}

	/// <summary>
	/// Instantiation of forge Models
	/// </summary>
	/// <param name="info">Model Information</param>
	/// <param name="urn">Forge URL</param>
	/// <param name="sceneID">Forge Scene ID</param>
	/// <param name="bearer">Forge Token</param>
	/// <param name="loadMetaData"></param>
	/// <param name="loadMesh"></param>
	/// <param name="createColliders"></param>
	void SpawnForgeModel(ModelInfo info, string urn, string sceneID, string bearer, bool loadMetaData = true, bool loadMesh = true, bool createColliders = true)
	{

		GameObject parent = new GameObject("Container - " + info.modelName);

		// Rellenamos los datos de la instancia de forge

		if (prefabs.forgeInstance.TryGetComponent(out ForgeLoader forgeLoader))
		{

			forgeLoader.URN = urn; // URN => URL
			forgeLoader.SCENEID = sceneID;
			forgeLoader._BEARER = bearer; // Token
			forgeLoader.LoadMetadata = loadMetaData;
			forgeLoader.LoadMesh = loadMesh;
			forgeLoader.CreateColliders = createColliders;
		}

		GameObject forgeInstance = Instantiate(prefabs.forgeInstance, UIReferences.Instance.forgeContainer);
		
		forgeInstance.transform.position = currentPosition;
		forgeInstance.transform.rotation = Quaternion.Euler(currentRotation);
		forgeInstance.transform.localScale = currentScale;

		forgeInstance.name = "Forge Loader - " + info.modelName;
		forgeInstance.transform.parent = parent.transform;

		parent.transform.parent = UIReferences.Instance.forgeContainer.transform;

		if (!info.set)
			parent.transform.parent = SpawnedObject.transform;


		parent.layer = LayerMask.NameToLayer("WorldGeometry");
		currentObjectsInScene.Add(parent);
	}

	#endregion

	#region TriLib Importer Events
	private void OnError(IContextualizedError obj)
	{
		throw new NotSupportedException("Object download failed!");
	}

	private void OnProgress(AssetLoaderContext arg1, float arg2)
	{
		
		fill.fillAmount = arg2;

		if (arg2 >= 1)
		{
			UIReferences.Instance.loadingBar.SetActive(false);
		}
	}

	private void OnMaterialsLoad(AssetLoaderContext obj)
	{
		SpawnedObject.transform.position = currentPosition;
		SpawnedObject.transform.rotation = Quaternion.Euler(currentRotation);
		SpawnedObject.transform.localScale = currentScale;

		isDownloading = false;
	}

	private void OnLoad(AssetLoaderContext obj)
	{
		
	}

	#endregion

	#region Model/KPI editor

	public void PlaceModel(GameObject obj, int index)
	{
		StartCoroutine(PlaceModelCoroutine(obj, index));
	}
	IEnumerator PlaceModelCoroutine(GameObject obj, int index)
	{
		yield return new WaitUntil(() => !isDownloading);
	
		ModelEditor.Instance.gameObject.SetActive(true);
		ModelEditor.Instance.SetUp(obj, index); // Activamos el editor
	}

	public void PlaceKPI(GameObject kpi, ModelInfo parentModel, KPIProperty property)
	{
		ModelEditor.Instance.gameObject.SetActive(true);
		ModelEditor.Instance.SetUp(kpi, parentModel,property); // Activamos el editor
	}

	#endregion

	#region Data loading
	void LoadKPI()
	{
		UIReferences.Instance.dlButton.interactable = false;

		string [] kpiNames = FileSystem.GetFolderNames(FileSystem.kpis).ToArray();
		kpiList.Clear();


		foreach (Transform t in UIReferences.Instance.panelToSpawnKPI)
			Destroy(t.gameObject);

		foreach (string fileName in kpiNames)
		{
			KPIProperty property = FileSystem.ImportDataFromCustomFolder<KPIProperty>(FileSystem.kpis,fileName, fileName);
			kpiList.Add(property);
		}

		StartCoroutine(LoadModels());
	}
	IEnumerator LoadModels()
	{
		notificationCount = 0;

		List<string> modelNames = FileSystem.GetFolderNames(FileSystem.models);
		models.Clear();

		foreach (GameObject g in currentObjectsInScene)
			Destroy(g);

		foreach(Transform g in UIReferences.Instance.graphContainer)
		{
			Destroy(g.gameObject);
		}

		currentObjectsInScene.Clear();

		foreach (Transform t in UIReferences.Instance.panelToSpawnModels)
			Destroy(t.gameObject);


		foreach (string fileName in modelNames)
		{
			ModelInfo info = FileSystem.ImportDataFromCustomFolder<ModelInfo>(FileSystem.models,fileName, fileName);
			models.Add(info);
		}

		foreach (ModelInfo info in models)
		{

			if (info.hasModel && info.set)
			{

				// Descomentar lo siguiente si se quiere que se descarguen los modelos cada vez que se inicia

				if(info.storageType == StorageType.Server)
				{
					currentModel = info;
					/*SpawnedObject = new GameObject(info.modelName);

					SpawnProp(info.modelURL);
					isDownloading = true;

					currentPosition = new Vector3(info.position.Value.x, info.position.Value.y, info.position.Value.z);
					currentRotation = new Vector3(info.rotation.Value.x, info.rotation.Value.y, info.rotation.Value.z);
					currentScale = new Vector3(info.scale.Value.x, info.scale.Value.y, info.scale.Value.z);

					yield return new WaitUntil(() => !isDownloading);*/

					yield return null; 

					/*RecursiveColliderAdd(SpawnedObject.transform, info);

					currentObjectsInScene.Add(SpawnedObject);*/
				}

				else
				{
					if (info.position.HasValue && info.rotation.HasValue && info.scale.HasValue)
					{
						currentPosition = new Vector3(info.position.Value.x, info.position.Value.y, info.position.Value.z);
						currentRotation = new Vector3(info.rotation.Value.x, info.rotation.Value.y, info.rotation.Value.z);
						currentScale = new Vector3(info.scale.Value.x, info.scale.Value.y, info.scale.Value.z);

						currentModel = info;
					}

					SpawnForgeModel(info, info.modelURL, info.scene, userData.Access_Token);
				}
			}

			LoadProgress += 100f / models.Count; // Porcentaje

			if (info.hasModel && !info.set)
				IncreaseNotifications();
		}

		foreach (ModelInfo model in models)
		{
			if (model.kpis != null && model.kpis.Count > 0)
			{
				for(int i = 0; i < model.kpis.Count; i++)
				{
					model.kpis[i] = SetKPIProperties(model.kpis[i].id, model, model.kpis[i]);

					if(!model.kpis[i].set)
						IncreaseNotifications();
				}
			}
		}

		UIReferences.Instance.SetNotifications(notificationCount);

		currentPosition = new Vector3(0, 0, 0);
		currentRotation = new Vector3(0, 0, 0);
		currentScale = new Vector3(1, 1, 1);

		if(models.Count > 0)
		{
			LevelManager.Instance.SetMaxLevel();
			SpawnButtons();

			UIReferences.Instance.FPSButton.gameObject.SetActive(true);
		}
		else
		{
			Debug.Log("No models detected to Load"); // TODO: Mostrar una ventana de informacion
		}
	}

	#endregion

	#region Button Instancing
	void SpawnButtons()
	{
		nestedModel.Clear();

		foreach (Transform t in UIReferences.Instance.panelToSpawnParents)
			Destroy(t.gameObject);

		foreach (ModelInfo model in models)
		{
			if (int.Parse(model.level) == 1) // Parents
			{
				nestedModel.Add(model);
			}
		}

		for (int i = 0; i < models.Count; i++)
		{
			if (int.Parse(models[i].level) > 1) // Si es hijo
			{
				ModelInfo dad = GetModel(int.Parse(models[i].idDad)); // Se coge el padre y se le añade a sus hijos

				if (dad.children == null)
					dad.children = new List<ModelInfo>();

				dad.children.Add(models[i]);
			}
		}

		foreach (ModelInfo model in nestedModel) // Por cada padre se itera por sus hijos
		{
			TraverseModel(model);
		}

		UIReferences.Instance.dlButton.interactable = true;
	}

	#endregion

	#region Model Instancing
	void TraverseModel(ModelInfo info) // Iterador recursivo
	{

		if (int.Parse(info.level) < 2) // Padre
		{
			GameObject g = Instantiate(prefabs.buttonParent, UIReferences.Instance.panelToSpawnParents);
			g.name = "btn" + info.name;
			g.GetComponent<ButtonManagerWithIcon>().buttonText = info.name;
			
			g.GetComponent<Button>().onClick.AddListener(() =>
			{

				currentModel = info;

				foreach (Transform t in UIReferences.Instance.dropdownContainer) 
					Destroy(t.gameObject);

				GameObject dropdownGO = Instantiate(prefabs.dropdown, UIReferences.Instance.dropdownContainer);
				List<Item> itemList = new List<Item>();

				
				foreach (ModelInfo model in models) 
				{
					if(model.idDad != null && int.Parse(model.idDad) == info.id && model.set)
					{
						Item item = new Item
						{
							itemIcon = null,
							itemName = model.name,
							OnItemSelection = new UnityEvent()
						};

						item.OnItemSelection.AddListener(() => DropdownFunc(model));

						itemList.Add(item);
					}
				}

				if(dropdownGO.TryGetComponent(out CustomDropdown customDropdown))						
					customDropdown.dropdownItems = itemList;


				if (info.position.HasValue)
				{
					CameraManager.Instance.MoveCamera(new Vector3(info.position.Value.x, info.position.Value.y, info.position.Value.z));
				}
					
				
			});

			GameObject unpositionedModel = null;

			if (!info.set && info.hasModel)
			{
				unpositionedModel = Instantiate(prefabs.buttonSinColocar, UIReferences.Instance.panelToSpawnModels);
				unpositionedModel.name = "btn" + info.name;
				unpositionedModel.GetComponent<ButtonManagerWithIcon>().buttonText = info.modelName;
			}

			if (info.hasKPI())
			{
				foreach(KPIProperty kpi in info.kpis)
				{
					if (!kpi.set)
					{
						GameObject kpiButton = Instantiate(prefabs.buttonSinColocar, UIReferences.Instance.panelToSpawnKPI);

						kpiButton.GetComponent<ButtonManagerWithIcon>().buttonText = kpi.id_kpi.ToString();

						KPIFunc(info, kpi, kpiButton);
					}
					else
					{
						LoadGraph(kpi.graphType, info, kpi);
					}
				}
			}

			if (unpositionedModel != null)
			{
				StartCoroutine(ModelFunc(info, unpositionedModel));
			}

			else
			{
				StartCoroutine(ModelFunc(info, g));
			}
				
		}

		else // Hijos
		{

			if (!info.set && info.hasModel)
			{
				GameObject g = Instantiate(prefabs.buttonSinColocar, UIReferences.Instance.panelToSpawnModels);
				
				g.name = "btn" + info.name;

				g.GetComponent<ButtonManagerWithIcon>().buttonText = info.modelName;
				StartCoroutine(ModelFunc(info, g));		
			}

			if (info.hasKPI())
			{
				foreach (KPIProperty kpi in info.kpis)
				{
					if (!kpi.set)
					{
						GameObject kpiButton = Instantiate(prefabs.buttonSinColocar, UIReferences.Instance.panelToSpawnKPI);

						kpiButton.GetComponent<ButtonManagerWithIcon>().buttonText = kpi.id_kpi.ToString();

						KPIFunc(info, kpi, kpiButton);
					}
					else
					{
						LoadGraph(kpi.graphType, info, kpi);
					}
				}
			}

			if (info.hasChildren())
			{
				foreach (ModelInfo son in info.children)
				{
					TraverseModel(son);
				}
			}
		}
	}

	void DropdownFunc(ModelInfo info)
	{
		CameraManager.Instance.MoveCamera(new Vector3(info.position.Value.x, info.position.Value.y, info.position.Value.z));
	}
	IEnumerator PositionModel(ModelInfo info)
	{
		SpawnedObject = new GameObject(info.name);


		switch (info.storageType)
		{
			case StorageType.Forge:
				SpawnForgeModel(info, info.modelURL, info.scene, userData.Access_Token);
				break;
			
			case StorageType.Server:
				SpawnProp(info.modelURL);
				break;
			
			default:
				break;
		}
		
		StartCoroutine(PlaceModelCoroutine(SpawnedObject, info.id));
		currentObjectsInScene.Add(SpawnedObject);

		yield return new WaitUntil(() => !isDownloading);

		if (info.storageType == StorageType.Server)
			RecursiveColliderAdd(SpawnedObject.transform, info);
	}

	#endregion

	#region KPI & Model Events
	void KPIFunc(ModelInfo parent, KPIProperty kpi, GameObject go)
	{
		UnityEvent onClickEvent = go.GetComponent<Button>().onClick;

		
		onClickEvent.AddListener(() =>
		{
				LoadGraph(kpi.graphType, parent, kpi);
				Destroy(go);
		});
	}

	IEnumerator ModelFunc(ModelInfo info, GameObject go)
	{

		UnityEvent onClickEvent = go.GetComponent<Button>().onClick;
		
		// Models

		if (info.hasModel && !info.set)
		{

			onClickEvent.AddListener(() =>
			{
				currentModel = info;
				StartCoroutine(PositionModel(info));
				Destroy(go);
			});
	
		}
		
		if (!info.hasChildren())
			yield break;

		yield return new WaitForSeconds(0.001f);

		foreach (ModelInfo model in info.children)
		{
			TraverseModel(model); // Vuelve a iterar por sus hijos
		}
	}

	#endregion

	#region Notifications Management
	public void DecreaseNotifications()
	{
		if (notificationCount > 0)
			notificationCount--;

		UIReferences.Instance.SetNotifications(notificationCount);
	}

	public void IncreaseNotifications()
	{
		notificationCount++;

		UIReferences.Instance.SetNotifications(notificationCount);
	}

	#endregion

	#region Graph Instancing
	void LoadGraph(GraphType type, ModelInfo parentModel, KPIProperty property)
	{
		GameObject graphInstance = null;

		switch (type)
		{
			case GraphType.Boolean:
				graphInstance = Instantiate(graphs.boolean, UIReferences.Instance.graphContainer);
				break;
			case GraphType.Flow:
				graphInstance = Instantiate(graphs.flow, UIReferences.Instance.graphContainer);
				break;
			case GraphType.Levels:
				graphInstance = Instantiate(graphs.levels, UIReferences.Instance.graphContainer);
				break;
			case GraphType.Number:
				graphInstance = Instantiate(graphs.number, UIReferences.Instance.graphContainer);
				break;
			case GraphType.Pressure:
				graphInstance = Instantiate(graphs.pressure, UIReferences.Instance.graphContainer);
				break;
			case GraphType.Speed:
				graphInstance = Instantiate(graphs.speed, UIReferences.Instance.graphContainer);
				break;
			case GraphType.Temperature:
				graphInstance = Instantiate(graphs.temperature, UIReferences.Instance.graphContainer);
				break;
			case GraphType.Time:
				graphInstance = Instantiate(graphs.time, UIReferences.Instance.graphContainer);
				break;
		}

		if(graphInstance != null)
		{
			graphInstance.name = $"Model - {parentModel.modelName} // KPI - {property.name}";

			if (graphInstance.TryGetComponent(out GraphFunc func))
				func.SetUp(ModelEditor.Instance, parentModel, property);


			if (!property.position.HasValue && !property.rotation.HasValue && !property.scale.HasValue)
				PlaceKPI(graphInstance, parentModel, property);
			else
			{
				graphInstance.transform.position = new Vector3(property.position.Value.x, property.position.Value.y, property.position.Value.z);
				graphInstance.transform.rotation = Quaternion.Euler(new Vector3(property.rotation.Value.x, property.rotation.Value.y, property.rotation.Value.z));
				graphInstance.transform.localScale = new Vector3(property.scale.Value.x, property.scale.Value.y, property.scale.Value.z);
			}

			
		}
		else
		{
			throw new NullReferenceException();
		}
	}

	#endregion

	public KPIProperty SetKPIProperties(int id, ModelInfo parentModel, KPIProperty property)
	{
		KPIProperty modifiedProperty = null;

		if (kpiList == null)
			return modifiedProperty;


		for (int i = 0; i < parentModel.kpis.Count; i++)
		{
			if (parentModel.kpis[i].id == id)
			{
				modifiedProperty = parentModel.kpis[i];
			}
		}
		

		if(modifiedProperty != null)
		{
			
			foreach(KPIProperty kpi in kpiList)
			{
				if (kpi.id == id)
					modifiedProperty = new KPIProperty(kpi.id, kpi.id_kpi, kpi.name, kpi.graphType.ToString().ToLower(), kpi.labelMin, kpi.labelMax, kpi.onlineMin, kpi.onlineMax, kpi.units,kpi.leyend);
			}

			if (property.position.HasValue && property.rotation.HasValue && property.scale.HasValue)
				modifiedProperty.Set(property.position.Value, property.rotation.Value, property.scale.Value);
		}

		return modifiedProperty;
	}

	public ModelInfo GetModel(int modelIndex)
	{
		foreach (ModelInfo info in models)
		{
			if (info.id == modelIndex)
				return info;
		}
		return null;
	}

	void RecursiveColliderAdd(Transform mainObj, ModelInfo info) // Añade collider a todo lo que tenga malla.
	{
		foreach (Transform g in mainObj)
		{
			if (g.GetComponent<MeshRenderer>() != null)
			{
				InteractableSceneObject interactable = g.gameObject.AddComponent<InteractableSceneObject>();
				interactable.SetUp(mainObj.gameObject, info);
			}

			RecursiveColliderAdd(g, info);
		}
	}
}
