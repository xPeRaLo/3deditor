﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.UI;


public enum GraphType { Boolean, Flow, Levels, Number, Pressure, Speed, Temperature, Time }
[Serializable]
public class KPIProperty
{
	
	public int id;
	public string id_kpi;
	public string name;

	// Label data

	public float labelMin;
	public float labelMax;

	// Graph data

	public float onlineMin;
	public float onlineMax;
	public string units;
	public string leyend;

	public GraphType graphType;

	public float3? position;
	public float3? rotation;
	public float3? scale;

	public bool set = false;
	public bool panel = false;


	public KPIProperty(int id)
	{
		this.id = id;
	}
	public KPIProperty(int id, string id_kpi, string name, string graph, float labelMin, float labelMax, float onlineMin, float onlineMax, string units, string leyend, bool panel = false)
	{
		this.id = id;
		this.name = name;
		this.panel = panel;
		this.id_kpi = id_kpi;
		this.labelMin = labelMin;
		this.labelMax = labelMax;
		this.onlineMin = onlineMin;
		this.onlineMax = onlineMax;
		this.units = units;
		this.leyend = leyend;

		switch (graph)
		{
			case "boolean":
				graphType = GraphType.Boolean;
				break;
			case "flow":
				graphType = GraphType.Flow;
				break;
			case "levels":
				graphType = GraphType.Levels;
				break;
			case "number":
				graphType = GraphType.Number;
				break;
			case "speed":
				graphType = GraphType.Speed;
				break;
			case "temperature":
				graphType = GraphType.Temperature;
				break;
			case "time":
				graphType = GraphType.Time;
				break;
			case "pressure":
				graphType = GraphType.Pressure;
				break;
		}
	}

	public void Set(float3? pos, float3? rot, float3? s)
	{
		position = new float3(pos.Value);
		rotation = new float3(rot.Value);
		scale = new float3(s.Value);

		set = true;
	}

	public void SetProperties()
	{

	}

	public KPIExport getKPIExport()
	{
		KPIExport export = new KPIExport(id, panel);

		if (set)
		{
			export.Set(new Vector3(position.Value.x, position.Value.y, position.Value.z),
					   new Vector3(rotation.Value.x, rotation.Value.y, rotation.Value.z),
					   new Vector3(scale.Value.x, scale.Value.y, scale.Value.z));
		}

		return export;
	}
}
