﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using SimpleJSON;
using Utils.FileSystem;
using Unity.Mathematics;
using System.IO;
using System;

public class DataDownload : MonoBehaviour
{
	#region Confidencial

	string urlModelos = "http://13.36.162.133:443/modelos/instalaciones/installation/";
	string urlKPIs = "http://13.36.162.133:443/modelos/instalaciones/kpi/";

	#endregion
	private void Awake()
	{
	
		StartCoroutine(FileImporter.Instance.userData.GetAutoDeskToken()); // Gets token
	}

	private void Start()
	{
		DownloadData(); // Descarga datos de la nube
		
		FileImporter.Instance.LoadStoredModels(); // Carga los modelos almacenados en el equipo
	}



	public void DownloadData()
	{

		// Para la descarga de nuevos modelos, sobreescribimos lo antigüo 

		if (Directory.Exists(FileSystem.models))
			Directory.Delete(FileSystem.models,true);

		if (Directory.Exists(FileSystem.kpis))
			Directory.Delete(FileSystem.kpis, true);

		Directory.CreateDirectory(FileSystem.models);
		Directory.CreateDirectory(FileSystem.kpis);

		
		StartCoroutine(KPIData());		
	}

	#region Requests all models from the server

	IEnumerator KPIData()
	{
		UnityWebRequest request = UnityWebRequest.Get(urlKPIs);

		yield return request.SendWebRequest();

		if (request.result == UnityWebRequest.Result.ConnectionError)
		{
			Debug.Log(request.error); // TODO: Mostrar una ventana de error
		}
		else
		{
			var parsedJSON = JSON.Parse(request.downloadHandler.text);

			#region Esto de momento no se usa, pero vendra bien si se quiere crear una barra de progreso
			
			float count = 100f / parsedJSON.AsArray.Count;
			float progress = 0f;

			#endregion

			foreach (JSONObject o in parsedJSON)
			{
				var id = o["id"];
				var id_kpi = o["id_kpi"];
				var name = o["name"];

				// Label data

				var labelMin = o["label_min"];
				var labelMax = o["label_max"];
				var graphType = o["label_type"]["type"];


				// Online data
				
				var leyend = o["label_data"][0]["leyend"];
				var units = o["label_data"][0]["units"];
				var onlineMin = o["online_min"];
				var onlineMax = o["online_max"];

				KPIProperty kpi = new KPIProperty(id, id_kpi, name,graphType, labelMin, labelMax, onlineMin, onlineMax, units, leyend);

				FileSystem.ExportDataToCustomFolder(FileSystem.kpis, kpi, kpi.id_kpi, kpi.id_kpi);

				progress += count;
			}
		}

		StartCoroutine(ModelData());
	}

	IEnumerator ModelData()
	{
		LevelManager.Instance.ClearInteractables();

		UnityWebRequest request = UnityWebRequest.Get(urlModelos);

		yield return request.SendWebRequest();

		if (request.result == UnityWebRequest.Result.ConnectionError)
		{
			Debug.Log(request.error); // TODO: Mostrar una ventana de error
		}
		else
		{
			// Show results as text
			var parsedJSON = JSON.Parse(request.downloadHandler.text);

			foreach(JSONObject o in parsedJSON)
			{
				// Instalacion

				var id = o["id"];
				var name = o["name"];
				var level = o["level"];
				var order = o["order"];
				var idDad = o["idDad"];

				// Modelo 3D

				var modelName = o["model3d"]["name"]; ;
				var modelURL = o["model3d"]["storage"]["content"]["url"]; ;
				var forgeURN = o["model3d"]["storage"]["content"]["urn"]; ;
				var forgeScene = o["model3d"]["storage"]["content"]["scene"];
				var forgeAccessToken = o["model3d"]["storage"]["content"]["access_token"];

				var type = o["model3d"]["storage"]["type"];
				var thumbnail = o["model3d"]["thumbnail"];

				var position = o["model3d"]["position"];
				var rotation = o["model3d"]["rotation"];
				var scale = o["model3d"]["scale"];
				
				// KPIs
				var kpis = o["KPIs"];

				ModelInfo modelInfo;

				// De momento se pasa de los modelos de forge para tener mas facilidad con el debugging, descomentar ELSE

				if (type == "server")
					modelInfo = new ModelInfo(name, level, order, idDad, modelName, modelURL, id, thumbnail);
				else
					continue; // Eliminar esto
					//modelInfo = new ModelInfo(name, level, order, idDad, modelName, forgeScene, forgeURN, id,thumbnail);


				if (modelInfo.hasModel)
				{

					if (position.Count > 0 && rotation.Count > 0 && scale.Count > 0)
					{
	
						modelInfo.Set(new float3(position[0], position[1], position[2]),
									  new float3(rotation[0], rotation[1], rotation[2]),
									  new float3(scale[0], scale[1], scale[2]));
					}
				}

				if(kpis.Count > 0 && kpis != null)
				{
					foreach(JSONObject kpi in kpis)
					{

						var kpiID = kpi["kpi"];
						var kpiPosition = kpi["position"];
						var kpiRotation = kpi["rotation"];
						var kpiScale = kpi["scale"];
						var kpiPanel = kpi["panel"].AsBool;

						KPIProperty property = new KPIProperty(kpiID);
						property.panel = kpiPanel;	
						
						if (kpiPosition.Count > 0 && kpiRotation.Count > 0 && kpiScale.Count > 0)
						{
							property.Set(new Vector3(kpiPosition[0], kpiPosition[1], kpiPosition[2]),
										 new Vector3(kpiRotation[0], kpiRotation[1], kpiRotation[2]),
										 new Vector3(kpiScale[0], kpiScale[1], kpiScale[2]));

							
						}

						if (modelInfo.kpis == null)
							modelInfo.kpis = new List<KPIProperty>();

						modelInfo.kpis.Add(property);
					}
				}

				FileSystem.ExportDataToCustomFolder(FileSystem.models,modelInfo, name + "/", name);

			}
		}

		FileImporter.Instance.LoadData();
	}

	#endregion

	
}





