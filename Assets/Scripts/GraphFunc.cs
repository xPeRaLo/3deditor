using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Utils.Math;

[RequireComponent(typeof(BoxCollider))]
public class GraphFunc : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{

	ModelInfo parentModel;
	KPIProperty property;
	ModelEditor editor;

	Vector3 normalScale = new Vector3(1,1,1);
	Vector3 hoverScale;

	bool hovered = false;
	bool filling = true;

	float timer = 0f;

	float random = 0f;

	Image graphFill;
	TextMeshProUGUI graphText;

	void Awake()
	{
		//GetComponent<Canvas>().worldCamera = GameObject.Find("UICamera").GetComponent<Camera>();
		GetComponent<BoxCollider>().size = GetComponent<RectTransform>().sizeDelta;

		graphFill = gameObject.transform.Find("RELLENO").GetComponent<Image>();
		graphText = gameObject.transform.Find("VALOR").GetComponent<TextMeshProUGUI>();
	}

	void Update()
	{
		RandomFill(5f);
	}
	public void OnPointerEnter(PointerEventData eventData)
	{
		hovered = true;
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		hovered = false;
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		editor.SetUp(gameObject, parentModel, property); // Al hacer click en un kpi se puede modificar su posicion
	}

	/// <summary>
	/// Sets up the grah so it can be moved
	/// </summary>
	/// <param name="editor"> Model Editor </param>
	/// <param name="parentModel"> Parent Model </param>
	/// <param name="property">Property to edit </param>
	public void SetUp(ModelEditor editor,ModelInfo parentModel, KPIProperty property)
	{
		this.parentModel = parentModel;
		this.property = property;
		this.editor = editor;

		if (property.set)
			normalScale = new Vector3(property.scale.Value.x, property.scale.Value.y, property.scale.Value.z);

		hoverScale = normalScale * 1.2f;
	}

	public void ModifySize(Vector3 size)
	{
		normalScale = size;
		hoverScale = normalScale * 1.2f;
	}

	/// <summary>
	/// Fills the graph with random values each x seconds
	/// </summary>
	/// <param name="seconds"> The amount of seconds the timer will remain active before filling the graph again </param>
	void RandomFill(float seconds)
	{
		if (filling && graphFill != null && graphText != null)
		{
			if (timer > 0f)
			{
				float normalized = MathFuncs.NormalizedValue(random, 0f, 50f);

				graphFill.fillAmount = Mathf.Lerp(graphFill.fillAmount, normalized, timer * Time.deltaTime);

				timer -= Time.deltaTime;
			}
			else
			{
				random = Random.Range(0f, 50f);
				graphText.text = random.ToString("F2");
				timer = seconds;
			}

		}
	}
}
