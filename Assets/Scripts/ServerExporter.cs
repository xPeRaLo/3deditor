﻿using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using SimpleJSON;
using UnityEngine.Networking;
using Utils.Singleton;

public class ServerExporter : Singleton<ServerExporter>
{
	const string URL = "http://13.36.162.133:443/modelos/instalaciones/installation/";

	public void ExportDataToServer(DataForExport data, int id)
	{
		StartCoroutine(Upload(data, id));
	}

	IEnumerator Upload(DataForExport data, int id)
	{
		string installationURL = URL + id+"/";

		var convertedJSON = JsonUtility.ToJson(data);
		byte[] byteJSON = new System.Text.UTF8Encoding().GetBytes(convertedJSON);

		var www = new UnityWebRequest(installationURL, "PATCH")
		{
			uploadHandler = new UploadHandlerRaw(byteJSON),
			downloadHandler = new DownloadHandlerBuffer()
		};

		www.SetRequestHeader("Content-Type", "application/json");

		yield return www.SendWebRequest();

		if (www.result == UnityWebRequest.Result.ConnectionError)
		{
			Debug.Log(www.error); // TODO: Mostrar ventana de error.
		}
		else
		{
			Debug.Log("Éxito"); // TODO: Mostrar ventana de confirmación
		}
	}

}

