using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using Utils.Exporter;

public class TestExport : MonoBehaviour
{

    List<ModelData> models = new List<ModelData>();


    void Start()
    {
        //FBXUtils.ExportGameObject(gameObject);
        models = FBXUtils.ImportGameObject("Fabrica");
        LoadModels();

    }

    void LoadModels()
    {
        GameObject parent = new GameObject("Parent");
        int i = 0;


        foreach(ModelData model in models)
		{
            GameObject mod = new GameObject(i.ToString());
            i++;
            

            foreach (MeshData m in model.MeshData)
            {
                GameObject obj = new GameObject(m.MeshName, typeof(MeshFilter), typeof(MeshRenderer), typeof(MeshCollider));
                obj.transform.position = m.Position;
                obj.transform.rotation = m.Rotation;
                obj.transform.localScale = m.Scale;

                obj.transform.SetParent(parent.transform);

                MeshRenderer renderer = obj.GetComponent<MeshRenderer>();
                MeshFilter filter = obj.GetComponent<MeshFilter>();
                MeshCollider collider = obj.GetComponent<MeshCollider>();
        
                if(CreateMesh(m, out Mesh mesh))
				{
                    filter.mesh = mesh;
                    collider.sharedMesh = mesh;
                    SetRenderer(renderer, m);
                }
                
                obj.transform.SetParent(mod.transform);
            }

            mod.transform.SetParent(parent.transform);
        }
        
    }

    bool CreateMesh(MeshData data, out Mesh mesh)
    {
		try
		{
            mesh = new Mesh()
            {
                vertices = data.Vertices,
                triangles = data.Triangles,
                normals = data.Normals,
                subMeshCount = data.SubMesheses.Count
            };

            for (int i = 0; i < data.SubMesheses.Count; i++)
            {
                SubMeshDescriptor subMesh = mesh.GetSubMesh(i);


                subMesh.baseVertex = data.SubMesheses[i].BaseVertex;
                subMesh.firstVertex = data.SubMesheses[i].FirstVertex;
                subMesh.indexCount = data.SubMesheses[i].IndexCount;
                subMesh.indexStart = data.SubMesheses[i].IndexStart;
                subMesh.topology = data.SubMesheses[i].Topology;
                subMesh.vertexCount = data.SubMesheses[i].VertexCount;

                Bounds bounds = new Bounds()
                {
                    center = data.SubMesheses[i].Bounds.Center,
                    extents = data.SubMesheses[i].Bounds.Extents,
                    size = data.SubMesheses[i].Bounds.Size,
                    max = data.SubMesheses[i].Bounds.Max,
                    min = data.SubMesheses[i].Bounds.Min
                };

                subMesh.bounds = bounds;

            }

			#region UV

			mesh.uv = data.MeshUV.UV1;
            mesh.uv2 = data.MeshUV.UV2;
            mesh.uv3 = data.MeshUV.UV3;
            mesh.uv4 = data.MeshUV.UV4;
            mesh.uv5 = data.MeshUV.UV5;
            mesh.uv6 = data.MeshUV.UV6;
            mesh.uv7 = data.MeshUV.UV7;
            mesh.uv8 = data.MeshUV.UV8;

			#endregion

			return true;
		}
		catch(Exception e)
		{
            // TODO: Mostrar popup error
            Debug.LogError(e.ToString());
            mesh = null;
            return false;
		}
         
    }

    void SetRenderer(MeshRenderer renderer, MeshData meshData)
	{
        Material[] materials = new Material[meshData.MaterialsList.Count];

        for(int i = 0; i < materials.Length; i++)
		{
            Material material = MaterialAttributes.SetMaterial(meshData.MaterialsList[i].Textures, meshData.MaterialsList[i].Properties, meshData.MaterialsList[i].Colors);

            #region ENABLE MATERIAL

            material.EnableKeyword("_NORMALMAP");
            material.EnableKeyword("_ALPHATEST_ON	");
            material.EnableKeyword("_ALPHABLEND_ON");
            material.EnableKeyword("_ALPHAPREMULTIPLY_ON");
            material.EnableKeyword("_EMISSION");
            material.EnableKeyword("_PARALLAXMAP");
            material.EnableKeyword("_DETAIL_MULX2");
            material.EnableKeyword("_METALLICGLOSSMAP");
            material.EnableKeyword("_SPECGLOSSMAP");

            #endregion

            materials[i] = material;
        }

        renderer.materials = materials;

        renderer.UpdateGIMaterials();
	}
}
        
  