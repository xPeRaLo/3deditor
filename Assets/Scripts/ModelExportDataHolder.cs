using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

[System.Serializable]
public class ModelExportDataHolder
{
	public ModelInfo info;
	public float3 position;
	public float3 rotation;
	public float3 scale;
	public GameObject pf;

	public ModelExportDataHolder(ModelInfo info, float3 position, float3 rotation, float3 scale, GameObject pf)
	{
		this.info = info;
		this.pf = pf;
		this.position = position;
		this.rotation = rotation;
		this.scale = scale;
	}
}
