using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

[JsonObject(IsReference = true)]
public class GO
{
    public string Name { get; private set; }
    public TransF Transform { get; private set; }
    public List<GO> Children { get; private set; }
    public List<Comp> Components { get; private set; }
    public GO(Transform t)
    {
        Name = t.name;
        Transform = new TransF(t.position, t.localScale, t.rotation);
        Components = new List<Comp>(t.GetComponents<Component>().Where(x => x.GetType() != typeof(Transform)).Select(x => new Comp(x.GetType().Name)));
        Children = new List<GO>();
        for (int i = 0; i < t.childCount; i++)
            Children.Add(new GO(t.GetChild(i)));
    }
    [JsonConstructor]
    public GO(string name, TransF transform, List<Comp> components, List<GO> children)
    {
        Name = name;
        Transform = transform;
        Components = components;
        Children = children;
    }
}
public class Comp
{
    public string Type { get; private set; }
    public Comp(string type)
    {
        Type = type;
    }
}
public class TransF
{
    public Vector3 Position { get; private set; }
    public Vector3 Scale { get; private set; }
    public Quaternion Rotation { get; private set; }
    public TransF(Vector3 position, Vector3 scale, Quaternion rotation)
    {
        Position = position;
        Scale = scale;
        Rotation = rotation;
    }
}