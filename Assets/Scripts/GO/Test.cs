using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Test : MonoBehaviour
{
    public GameObject model;
    private string JsonPath { get; } = Path.Combine(Application.streamingAssetsPath, "Test.json");
    void Start()
    {
        SerializeModel(model);
        Destroy(model);
        DeserializeModel();
    }
    private void SerializeModel(GameObject obj)
    {
        string json = JsonConvert.SerializeObject(new GO(obj.transform), Formatting.Indented);
        File.WriteAllText(JsonPath, json);
    }
    private void DeserializeModel()
    {
        GO obj = JsonConvert.DeserializeObject<GO>(File.ReadAllText(JsonPath));
        InstantiateGameObject(obj, null);
    }
    private void InstantiateGameObject(GO obj, Transform parent)
    {
        GameObject go = new GameObject(obj.Name);
        go.transform.parent = parent;
        go.transform.SetPositionAndRotation(obj.Transform.Position, obj.Transform.Rotation);
        go.transform.localScale = obj.Transform.Scale;
        

        foreach (var comp in obj.Components)
        {
            if (comp.Type == "ForgeProperties")
            {
                //go.AddComponent(Type.GetType($"Autodesk.Forge.ARKit.{comp.Type}, Autodesk.Forge.ARKit"));
            }
            else
            {
                go.AddComponent(Type.GetType($"UnityEngine.{comp.Type}, UnityEngine"));
            }
        }

        obj.Children.ForEach(x => InstantiateGameObject(x, go.transform));
    }
}