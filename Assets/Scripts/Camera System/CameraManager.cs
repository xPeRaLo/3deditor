﻿using System.Collections.Generic;
using UnityEngine;
using Utils.Singleton;

public class CameraManager : Singleton<CameraManager>
{
	Transform mainCameraTransform;
	Transform mainCamera;
	Transform UICamera;

	enum CameraDirection { Forward, Back, Return }
	CameraDirection direction = CameraDirection.Forward;

	Vector3 moveSpeed = Vector3.zero;
	Vector3? destination = null;
	
	Quaternion? lastTransformRotation = null;
	Quaternion? lastCameraRotation = null;
	Quaternion? lastUICameraRotation = null;

	const float REACH_DISTANCE = 7f;

	List<Vector3> cameraPositions = new List<Vector3>();
	List<Vector3> installationPositions = new List<Vector3>();


	[Range(0.1f, 0.6f)] [SerializeField] float smoothTime;
	[Range(0.0f, 10.0f)] [SerializeField] float maxSpeed;


	private void Awake()
	{
		mainCameraTransform = Camera.main.transform.parent;
		mainCamera = mainCameraTransform.GetChild(0);
		UICamera = mainCameraTransform.GetChild(1);
	}

	private void LateUpdate()
	{
		TranslateCamera();
	}

	void TranslateCamera()
	{
		if (destination.HasValue)
		{
			switch (direction)
			{
				case CameraDirection.Forward:


					mainCameraTransform.position = Vector3.SmoothDamp(mainCameraTransform.position,
															 new Vector3(destination.Value.x, 0.8f, destination.Value.z - REACH_DISTANCE),
															 ref moveSpeed, smoothTime, maxSpeed, Time.fixedDeltaTime);

					if (Vector3.Distance(mainCameraTransform.position, destination.Value) < REACH_DISTANCE)
					{
						moveSpeed = Vector3.zero;
						destination = null;
					}

					break;

					// Return to previous position on position list

				case CameraDirection.Back:

					mainCameraTransform.position = Vector3.SmoothDamp(mainCameraTransform.position, destination.Value, ref moveSpeed, smoothTime, maxSpeed, Time.fixedDeltaTime);

					if (Vector3.Distance(mainCameraTransform.position, destination.Value) <= 0.00001f)
					{
						mainCameraTransform.position = destination.Value;
						moveSpeed = Vector3.zero;
						destination = null;
					}

					break;

					// Return to Initial position from the free camera position

				case CameraDirection.Return:

					mainCameraTransform.position = Vector3.SmoothDamp(mainCameraTransform.position, destination.Value, ref moveSpeed, smoothTime, maxSpeed, Time.fixedDeltaTime);
					mainCameraTransform.localRotation = Quaternion.Slerp(mainCameraTransform.rotation, lastTransformRotation.Value, Time.fixedDeltaTime);
					mainCamera.localRotation = Quaternion.Slerp(mainCamera.localRotation , lastCameraRotation.Value, Time.fixedDeltaTime);
					UICamera.localRotation = Quaternion.Slerp(UICamera.localRotation, lastUICameraRotation.Value, Time.fixedDeltaTime);

					if (Vector3.Distance(mainCameraTransform.position, destination.Value) <= 0.00003)
					{
						mainCameraTransform.position = destination.Value;
						mainCameraTransform.localRotation = lastTransformRotation.Value;
						mainCamera.localRotation = lastCameraRotation.Value;
						UICamera.localRotation = lastUICameraRotation.Value;

						moveSpeed = Vector3.zero;
						destination = null;
						lastCameraRotation = null;
						lastTransformRotation = null;
						lastUICameraRotation = null;

						UIReferences.Instance.FPSButton.interactable = true;
					}

					break;
			}
		}
	}

	public void MoveCamera(Vector3 center)
	{
		if (installationPositions.Contains(center))
			return;
 		
		direction = CameraDirection.Forward;

		destination = center;

		cameraPositions.Insert(0,mainCameraTransform.position);
		installationPositions.Insert(0,center);
	}

	public void MoveCameraBack()
	{
		if (cameraPositions.Count < 1 || installationPositions.Count < 1)
			return;

		direction = CameraDirection.Back;

		destination = cameraPositions[0];

		cameraPositions.RemoveAt(0);
		installationPositions.RemoveAt(0);
	}

	public void ReturnToPosition(Vector3 destination, Quaternion lastTransformRotation, Quaternion lastCameraRotation, Quaternion lastUICameraRotation)
	{
		this.destination = destination;
		this.lastTransformRotation = lastTransformRotation;
		this.lastCameraRotation = lastCameraRotation;
		this.lastUICameraRotation = lastUICameraRotation;

		// Disable both free roam and return buttons 

		direction = CameraDirection.Return;
	}
}
