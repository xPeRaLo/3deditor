using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class FreeCameraMovement : MonoBehaviour
{
	
    Transform mainCamera;
	CameraManager camManager;
	FirstPersonController fps;
	
	Vector3 lastPosition;
	Quaternion lastMainCameraRotation;
	Quaternion lastMainCameraTransformRotation;

	Quaternion lastUICameraRotation; // UICamera


	private void Awake()
	{
		mainCamera = Camera.main.transform.parent;
		camManager = FindObjectOfType<CameraManager>();
		fps = GetComponent<FirstPersonController>();
	}

	private void OnEnable()
	{
		SaveLastProperties();

	}

	private void OnDisable()
	{
		LoadLastProperties();
		
	}

	void SaveLastProperties()
	{

		// Por algun motivo da null reference a pesar de tener la referencia, en algun sitio tiene que perderla

		//UIReferences.Instance.panelToSpawnParents.gameObject.SetActive(false);
		//UIReferences.Instance.dropdownContainer.gameObject.SetActive(false);

		fps.enabled = true;

		lastPosition = mainCamera.position;
		lastMainCameraTransformRotation = mainCamera.localRotation;
		lastMainCameraRotation = mainCamera.GetChild(0).localRotation;
		lastUICameraRotation = mainCamera.GetChild(1).localRotation;
	}

	void LoadLastProperties()
	{
	
		//UIReferences.Instance.panelToSpawnParents.gameObject.SetActive(true);
		//UIReferences.Instance.dropdownContainer.gameObject.SetActive(true);

		fps.enabled = false;

		camManager.ReturnToPosition(lastPosition, lastMainCameraTransformRotation, lastMainCameraRotation, lastUICameraRotation);
	}
}
