﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityStandardAssets.Characters.FirstPerson;

[RequireComponent(typeof(MeshRenderer), typeof(MeshFilter))]
public abstract class IModellerButton : MonoBehaviour, IPointerEnterHandler, IPointerDownHandler, IPointerUpHandler, IDragHandler // All geometry buttons must implement
{
	public enum Direction { Up, Sides, Depth }
	[SerializeField] protected float buttonSpeed = 5f;
	[SerializeField] protected Direction direction;

	protected Transform model;

	protected MeshRenderer mRenderer;

	FirstPersonController fps;
	FreeCameraMovement freeCam;

	//const float MAX_BUTTON_SPEED = 10f;

	protected Color normalColor;
	Color ClickColor;

	protected virtual void Awake()
	{ 
		model = transform.parent.parent.Find("Model");

		mRenderer = GetComponent<MeshRenderer>();

		normalColor = mRenderer.material.color;
		ClickColor = new Color(mRenderer.material.color.r, mRenderer.material.color.g, mRenderer.material.color.b, mRenderer.material.color.a * 2f);

		fps = FindObjectOfType<FirstPersonController>();
		freeCam = FindObjectOfType<FreeCameraMovement>();
	}

	public virtual void OnPointerDown(PointerEventData eventData)
	{
		mRenderer.material.color = ClickColor;
		
		#region Deshabilitar cursor

		Cursor.lockState = CursorLockMode.Confined;
		Cursor.visible = false;

		#endregion

		// Desactivamos el movimiento de camara mientras interactuamos con sus botones, ya que si no se hace muy incomodo editar

		if (freeCam.enabled)
			fps.enabled = false;
	}

	public virtual void OnPointerUp(PointerEventData eventData)
	{
		mRenderer.material.color = normalColor;

		#region Habilitar cursor

		Cursor.lockState = CursorLockMode.None;
		Cursor.visible = true;

		#endregion

		if (freeCam.enabled)
			fps.enabled = true;
	}

	public virtual void OnDrag(PointerEventData eventData)
	{
		DoAction(eventData.delta);
	}

	public virtual void OnPointerEnter(PointerEventData eventData){}

	protected abstract void DoAction(Vector2 multiplier);

	
}
