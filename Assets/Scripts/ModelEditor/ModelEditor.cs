﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Unity.Mathematics;
using Utils.FileSystem;
using UnityEngine.UI;
using UnityEditor;
using System;
using Utils.Singleton;
using UnityFBXExporter;

public class ModelEditor : Singleton<ModelEditor>
{
	[SerializeField] GameObject objectEditorParent;
	[SerializeField] GameObject editorWindow;

	[SerializeField] GameObject positionEditor;
	[SerializeField] GameObject rotationEditor;
	[SerializeField] GameObject scaleEditor;

	[SerializeField] GameObject positionPanel;
	[SerializeField] GameObject rotationPanel;
	[SerializeField] GameObject scalePanel;

	[SerializeField] GameObject btnApplyModel;
	[SerializeField] GameObject btnApplyKPI;

	[Space]
	[Header("Model Editor Atributes")]

	[SerializeField] [Range(0,10)] int decimalsForPosition = 3; // Decimales para los textos de posicion
	[SerializeField] [Range(0,10)] int decimalsForRotation = 2; // Decimales para los textos de rotacion
	[SerializeField] [Range(0,10)] int decimalsForScale = 6; // Decimales para los textos de escala

	#region Texts 

	TMP_InputField xPosition;
	TMP_InputField yPosition;
	TMP_InputField zPosition;

	TMP_InputField xRotation;
	TMP_InputField yRotation;
	TMP_InputField zRotation;

	TMP_InputField xScale;
	TMP_InputField yScale;
	TMP_InputField zScale;

	#endregion

	GameObject _currentEditedObject;
	public GameObject CurrentEditedObject
	{
		get { return _currentEditedObject; }
		private set { _currentEditedObject = value; }
	}

	int indexEdited;

	ModelInfo currentModel;
	KPIProperty currentKPI;

	IInteractableObject[] interactables;
	GraphFunc [] graphs;

	private void Awake()
	{
		UIPositioningTexts();
	}

	/// <summary>
	/// Model Editor
	/// </summary>
	public void SetUp(GameObject obj, int index)
	{
		StartCoroutine(SetUpCoroutine(obj, index));
	}

	/// <summary>
	/// KPI Editor
	/// </summary>
	public void SetUp(GameObject objectToEdit, ModelInfo parentModel, KPIProperty kpi)
	{
		SetUpCoroutine(objectToEdit, parentModel,kpi);
	}

	void SetUpCoroutine(GameObject obj, ModelInfo parentModel, KPIProperty kpi)
	{
		btnApplyModel.SetActive(false);
		btnApplyKPI.SetActive(true);

		currentKPI = kpi;
		currentModel = parentModel;

		LevelManager.Instance.CurrentLevel = LevelManager.Instance.MaxLevel;

		UIReferences.Instance.panelToSpawnModels.gameObject.SetActive(false);
		UIReferences.Instance.panelToSpawnKPI.gameObject.SetActive(false);

		CurrentEditedObject = obj;

		if (CurrentEditedObject.TryGetComponent(out BoxCollider col))
			col.enabled = false;

		objectEditorParent.SetActive(true);

		objectEditorParent.transform.Find("PosArrows").localScale = new Vector3(1, 1, 1);

		objectEditorParent.transform.position = CurrentEditedObject.transform.position;
		objectEditorParent.transform.Find("Model").rotation = CurrentEditedObject.transform.rotation;
		objectEditorParent.transform.Find("Model").localScale = CurrentEditedObject.transform.lossyScale;

		CurrentEditedObject.transform.SetParent(objectEditorParent.transform.Find("Model"));

		editorWindow.SetActive(true);

		positionPanel.SetActive(true);
		rotationPanel.SetActive(false);
		scalePanel.SetActive(false);

		#region Disable interactables while editing

		interactables = FindObjectsOfType<IInteractableObject>();
		graphs = FindObjectsOfType<GraphFunc>();

		foreach (InteractableSceneObject i in interactables)
			i.Interactable(false);

		foreach(GraphFunc func in graphs)
		{
			if (func.gameObject.TryGetComponent(out BoxCollider collider))
				collider.enabled = false;
		}

		#endregion
	}
	IEnumerator SetUpCoroutine(GameObject obj, int index)
	{

		btnApplyModel.SetActive(true);
		btnApplyKPI.SetActive(false);

		LevelManager.Instance.CurrentLevel = LevelManager.Instance.MaxLevel; // Establecemos el nivel de vista al maximo posible, ya que interesa ver todas las instalaciones 																							   // para hacer una buena colocacion

		UIReferences.Instance.panelToSpawnModels.gameObject.SetActive(false);
		UIReferences.Instance.panelToSpawnKPI.gameObject.SetActive(false);

		CurrentEditedObject = obj;
		indexEdited = index;

		objectEditorParent.SetActive(true);

		objectEditorParent.transform.Find("PosArrows").localScale = new Vector3(1, 1, 1);

		objectEditorParent.transform.position = CurrentEditedObject.transform.position;
		objectEditorParent.transform.Find("Model").rotation = CurrentEditedObject.transform.rotation;
		objectEditorParent.transform.Find("Model").localScale = CurrentEditedObject.transform.lossyScale;

		CurrentEditedObject.transform.parent = objectEditorParent.transform.Find("Model");

		editorWindow.SetActive(true);

		EnablePositionEditor();

		yield return new WaitUntil(() => CurrentEditedObject.transform.childCount > 0); // Esperamos a que al menos haya algo descargado

		#region Disable interactables while editing

		interactables = FindObjectsOfType<IInteractableObject>();
		graphs = FindObjectsOfType<GraphFunc>();

		foreach (InteractableSceneObject i in interactables)
			i.Interactable(false);

		foreach (GraphFunc func in graphs)
		{
			if (func.gameObject.TryGetComponent(out BoxCollider collider))
				collider.enabled = false;
		}

		#endregion
	}

	private void Update()
	{
		if (CurrentEditedObject != null && CurrentEditedObject.transform.parent != null)
		{
			UpdateTexts();
		}
	}

	/// <summary>
	/// Updates all the texts with corresponding value of position, rotation, scale
	/// </summary>
	void UpdateTexts()
	{

		xPosition.text = CurrentEditedObject.transform.parent.parent.position.x.ToString($"F{decimalsForPosition}"); 
		yPosition.text = CurrentEditedObject.transform.parent.parent.position.y.ToString($"F{decimalsForPosition}");
		zPosition.text = CurrentEditedObject.transform.parent.parent.position.z.ToString($"F{decimalsForPosition}");

		xRotation.text = CurrentEditedObject.transform.parent.eulerAngles.x.ToString($"F{decimalsForRotation}");
		yRotation.text = CurrentEditedObject.transform.parent.eulerAngles.y.ToString($"F{decimalsForRotation}");
		zRotation.text = CurrentEditedObject.transform.parent.eulerAngles.z.ToString($"F{decimalsForRotation}");

		xScale.text = CurrentEditedObject.transform.parent.localScale.x.ToString($"F{decimalsForScale}");
		yScale.text = CurrentEditedObject.transform.parent.localScale.y.ToString($"F{decimalsForScale}");
		zScale.text = CurrentEditedObject.transform.parent.localScale.z.ToString($"F{decimalsForScale}");
	}


	/// <summary>
	/// Activates the position editor
	/// </summary>
	public void EnablePositionEditor()
	{
		positionPanel.SetActive(true);
		rotationPanel.SetActive(false);
		scalePanel.SetActive(false);

		positionEditor.SetActive(true);
		rotationEditor.SetActive(false);
		scaleEditor.SetActive(false);
	}

	/// <summary>
	/// Activates the rotation Editor
	/// </summary>
	public void EnableRotationEditor()
	{
		positionPanel.SetActive(false);
		rotationPanel.SetActive(true);
		scalePanel.SetActive(false);

		positionEditor.SetActive(false);
		rotationEditor.SetActive(true);
		scaleEditor.SetActive(false);
	}

	/// <summary>
	/// Activates the scale editor
	/// </summary>
	public void EnableScaleEditor()
	{
		positionPanel.SetActive(false);
		rotationPanel.SetActive(false);
		scalePanel.SetActive(true);

		positionEditor.SetActive(false);
		rotationEditor.SetActive(false);
		scaleEditor.SetActive(true);
	}

	public void SaveModel()
	{
		EnablePositionEditor();

		float3 position = new float3(CurrentEditedObject.transform.position.x, CurrentEditedObject.transform.position.y, CurrentEditedObject.transform.position.z);
		float3 rotation = new float3(CurrentEditedObject.transform.eulerAngles.x, CurrentEditedObject.transform.eulerAngles.y, CurrentEditedObject.transform.eulerAngles.z);
		float3 scale = new float3(CurrentEditedObject.transform.lossyScale.x, CurrentEditedObject.transform.lossyScale.y, CurrentEditedObject.transform.lossyScale.z);

		ModelInfo info = FileImporter.Instance.GetModel(indexEdited);

		if (!info.set)
			FileImporter.Instance.DecreaseNotifications();

		info.Set(position, rotation, scale);

		FBXExporter.ExportGameObjToFBX(CurrentEditedObject, Application.dataPath + $"/Resources/{info.modelName}.fbx", true, true);
		FileImporter.Instance.exportedModels.models.Add(new ModelExportDataHolder(info, position, rotation, scale,Resources.Load<GameObject>($"{info.modelName}") as GameObject));

		#region Server Export

		ModelProperty modelProperty = new ModelProperty(position, rotation, scale);

		List<KPIExport> propiedades = new List<KPIExport>();

		if (info.hasKPI())
		{
			foreach(KPIProperty p in info.kpis)
			{
				propiedades.Add(p.getKPIExport());
			}
		}

		KPIExport[] kPIProperties = propiedades.ToArray(); 

		DataForExport exportation = new DataForExport(modelProperty, kPIProperties);

		ServerExporter.Instance.ExportDataToServer(exportation, info.id);

		#endregion

		FileSystem.ExportDataToCustomFolder(FileSystem.models,info, info.name + "/", info.name);

		UIReferences.Instance.panelToSpawnModels.gameObject.SetActive(true);
		UIReferences.Instance.panelToSpawnKPI.gameObject.SetActive(true);


		CurrentEditedObject.transform.parent = null;

		#region Re-enable interactables

		interactables = FindObjectsOfType<InteractableSceneObject>();
		graphs = FindObjectsOfType<GraphFunc>();

		foreach (InteractableSceneObject i in interactables)
			i.Interactable(true);

		foreach (GraphFunc func in graphs)
		{
			if (func.gameObject.TryGetComponent(out BoxCollider collider))
				collider.enabled = true;
		}

		#endregion

		objectEditorParent.SetActive(false);
		editorWindow.SetActive(true);

		UIReferences.Instance.panelToSpawnModels.gameObject.SetActive(true);
		LevelManager.Instance.CurrentLevel = LevelManager.Instance.PreviousLevel;
	}

	public void SaveKPI()
	{
		EnablePositionEditor();

		float3 position = new float3(CurrentEditedObject.transform.position.x, CurrentEditedObject.transform.position.y, CurrentEditedObject.transform.position.z);
		float3 rotation = new float3(CurrentEditedObject.transform.eulerAngles.x, CurrentEditedObject.transform.eulerAngles.y, CurrentEditedObject.transform.eulerAngles.z);
		float3 scale = new float3(CurrentEditedObject.transform.lossyScale.x, CurrentEditedObject.transform.lossyScale.y, CurrentEditedObject.transform.lossyScale.z);

		if (!currentKPI.set)
			FileImporter.Instance.DecreaseNotifications();

		currentKPI.Set(position, rotation, scale);

		#region Server export

		ModelProperty modelProperty;

		if (currentModel.position.HasValue && currentModel.rotation.HasValue && currentModel.scale.HasValue)
			modelProperty = new ModelProperty(currentModel.position.Value, currentModel.rotation.Value, currentModel.scale.Value);
		else
			modelProperty = new ModelProperty(null, null, null);

		List<KPIExport> propiedades = new List<KPIExport>();

		if (currentModel.hasKPI())
		{
			foreach (KPIProperty p in currentModel.kpis)
			{
				propiedades.Add(p.getKPIExport());
			}
		}

		KPIExport[] kPIProperties = propiedades.ToArray();

		DataForExport exportation = new DataForExport(modelProperty, kPIProperties);

		ServerExporter.Instance.ExportDataToServer(exportation, currentModel.id);

		#endregion

		FileSystem.ExportDataToCustomFolder(FileSystem.models, currentModel, currentModel.name + "/", currentModel.name);

		UIReferences.Instance.panelToSpawnModels.gameObject.SetActive(true);
		UIReferences.Instance.panelToSpawnKPI.gameObject.SetActive(true);

		if (CurrentEditedObject.TryGetComponent(out GraphFunc currentGraph))
			currentGraph.ModifySize(CurrentEditedObject.transform.lossyScale);

		CurrentEditedObject.transform.SetParent(UIReferences.Instance.graphContainer);
		CurrentEditedObject = null;

		#region Re-enable Interactables

		interactables = FindObjectsOfType<InteractableSceneObject>();
		graphs = FindObjectsOfType<GraphFunc>();

		foreach (InteractableSceneObject i in interactables)
			i.Interactable(true);

		foreach (GraphFunc func in graphs)
		{
			if (func.gameObject.TryGetComponent(out BoxCollider collider))
				collider.enabled = true;
		}

		#endregion

		objectEditorParent.SetActive(false);
		editorWindow.SetActive(true);

		UIReferences.Instance.panelToSpawnModels.gameObject.SetActive(true);
		LevelManager.Instance.CurrentLevel = LevelManager.Instance.PreviousLevel;

		currentKPI = null;
		currentModel = null;

	}

	void SetLayer(GameObject g, int layer)
	{
		g.layer = layer;

		foreach(Transform obj in g.transform)
		{
			obj.gameObject.layer = layer;
			SetLayer(obj.gameObject, layer);
		}
	}

	void UIPositioningTexts()
	{
		xPosition = positionPanel.transform.Find("xPosition").GetComponent<TMP_InputField>();
		yPosition = positionPanel.transform.Find("yPosition").GetComponent<TMP_InputField>();
		zPosition = positionPanel.transform.Find("zPosition").GetComponent<TMP_InputField>();

		xRotation = rotationPanel.transform.Find("xRotation").GetComponent<TMP_InputField>();
		yRotation = rotationPanel.transform.Find("yRotation").GetComponent<TMP_InputField>();
		zRotation = rotationPanel.transform.Find("zRotation").GetComponent<TMP_InputField>();

		xScale = scalePanel.transform.Find("xScale").GetComponent<TMP_InputField>();
		yScale = scalePanel.transform.Find("yScale").GetComponent<TMP_InputField>();
		zScale = scalePanel.transform.Find("zScale").GetComponent<TMP_InputField>();
	}
}
