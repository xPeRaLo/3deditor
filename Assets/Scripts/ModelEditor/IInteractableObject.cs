﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public abstract class IInteractableObject : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{

	Collider objectCollider;
	[HideInInspector] public MeshRenderer objectRenderer;
	[HideInInspector] public ModelInfo info;

	GameObject parentObject;
	public bool IsActive { get { return objectRenderer.enabled && objectCollider.enabled; } }
	
	protected virtual void Awake()
	{
		objectCollider = gameObject.AddComponent<MeshCollider>();
		objectRenderer = gameObject.GetComponent<MeshRenderer>();

		LevelManager.Instance.AddInteractable(this);
	}

	public virtual void OnPointerClick(PointerEventData eventData)
	{
		// TODO: Check if zoom mode or edit mode is selected
		 

		/*FileImporter.Instance.PlaceModel(parentObject, info.id);  // Posiciona el modelo
		Interactable(false); // Quitamos la interaccion temporalmente

		LevelManager.Instance.CurrentLevel = int.Parse(info.level) + 1;

		if (LevelManager.Instance.CurrentLevel >= LevelManager.Instance.MaxLevel)
			LevelManager.Instance.CurrentLevel = LevelManager.Instance.MaxLevel;
		
		Vector3 position = new Vector3(info.position.Value.x, info.position.Value.y, info.position.Value.z);
		CameraManager.Instance.MoveCamera(position);*/
	}

	public virtual void OnPointerEnter(PointerEventData eventData)
	{
	}

	public virtual void OnPointerExit(PointerEventData eventData)
	{
		
	}

	public void SetUp(GameObject parent, ModelInfo info)
	{
		parentObject = parent.transform.root.gameObject;
		this.info = info;

		/*if(int.Parse(info.level) > LevelManager.Instance.CurrentLevel)
			Active(false);*/
		
	}

	public void Interactable(bool isInteractable)
	{
		IInteractableObject[] interactableSceneObjects = FindObjectsOfType<IInteractableObject>();

		foreach(IInteractableObject i in interactableSceneObjects)
		{
			i.enabled = isInteractable;
		}
	}

	public void Active(bool active)
	{
		if(objectRenderer != null && objectCollider != null)
		{
			objectRenderer.enabled = active;
			objectCollider.enabled = active;
		}

		// Zoom camera (?)
		// Spawn buttons (?)
	}
}
