﻿using Autodesk.Forge.ARKit;
using Newtonsoft.Json.Converters;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ScaleButton: IModellerButton
{

	Vector3 normalScale;
	Vector3 lastValidModelScale;
	Vector3 lastValidModelEditorScale;

	protected override void Awake()
	{
		base.Awake();

		normalScale = transform.localScale;

	}

	public override void OnDrag(PointerEventData eventData)
	{
		base.OnDrag(eventData);

		Vector2 delta = Vector2.one;

		if (eventData.delta.x < 0 ) // Raton hacia la izquierda => Se reduce por la mitad su tamaño
			delta.x = 0.5f;	
			
		else if (eventData.delta.x > 0 ) // Raton hacia la derecha => Se dobla su tamaño
			delta.x = 2f;
			
		else
			delta.x = 1f; // Raton en medio o no se mueve => Se mantiene
		

		DoAction(delta);
		

	}
	protected override void DoAction(Vector2 multiplier)
	{
		model.localScale = Vector3.Lerp(model.localScale, model.localScale * multiplier.x, Time.fixedDeltaTime);
		transform.localScale = Vector3.Lerp(transform.localScale, transform.localScale * multiplier.x, Time.fixedDeltaTime);

		#region Check para que no pase a escala 0 o negativa

		if (model.localScale.x > 0f && model.localScale.y > 0f && model.localScale.z > 0f)
		{
			lastValidModelScale = model.localScale;
			lastValidModelEditorScale = transform.localScale;
		}
		else
		{
			model.localScale = lastValidModelScale;
			transform.localScale = lastValidModelEditorScale;
		}

		#endregion
	}

	public override void OnPointerUp(PointerEventData eventData)
	{
		base.OnPointerUp(eventData);

		transform.localScale = normalScale; // Se reduce el tamaño del cubo indicador al original

		mRenderer.material.color = normalColor;

		#region Habilitar Cursor

		Cursor.lockState = CursorLockMode.None;
		Cursor.visible = true;

		#endregion
	}
}
