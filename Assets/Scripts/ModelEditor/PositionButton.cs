﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PositionButton : IModellerButton
{
	
	protected override void DoAction(Vector2 multiplier)
	{
			switch (direction)
			{
				case Direction.Depth:
					transform.parent.parent.position +=	model.parent.forward * (multiplier.y / 4) * buttonSpeed * Time.fixedDeltaTime; // Movemos con el eje Y del raton
					transform.parent.localScale += new Vector3(1,1,1) * (multiplier.y / 4) * buttonSpeed * Time.fixedDeltaTime;
				break;
				
				case Direction.Sides:
				transform.parent.parent.position += model.parent.right * (multiplier.x / 4) * buttonSpeed * Time.fixedDeltaTime; // Movemos con el eje X del raton
				break;

				case Direction.Up: 
					transform.parent.parent.position += model.parent.up * (multiplier.y / 4) * buttonSpeed * Time.fixedDeltaTime; // Movemos con el eje Y del raton
				break;
			}	
	}
}
