﻿using UnityEngine;

public class RotationButton : IModellerButton
{
	protected override void DoAction(Vector2 multiplier)
	{
		switch (direction)
		{
			case Direction.Depth:
				model.localRotation *= Quaternion.Euler(model.right * multiplier.y * buttonSpeed * Time.fixedDeltaTime); // Rotacion eje Y
				break;
			case Direction.Sides:
				model.localRotation *= Quaternion.Euler(model.forward * multiplier.x * -buttonSpeed * Time.fixedDeltaTime); // Rotacion eje X
				break;
			case Direction.Up:
				model.localRotation *= Quaternion.Euler(model.transform.up * multiplier.y * buttonSpeed * Time.fixedDeltaTime); // Rotacion eje Z
				break;
		}
	}
}
