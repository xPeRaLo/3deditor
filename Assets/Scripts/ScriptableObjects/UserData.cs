using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using SimpleJSON;

[CreateAssetMenu(fileName = "User Data", menuName = "Scriptable Objects/Data/User Data")]
public class UserData : ScriptableObject
{
	#region Confidencial

	public const string url = "https://developer.api.autodesk.com/authentication/v1/authenticate";
	public const string cliend_id = "zhs4dIBYEqwMlhtV9GMWfUArZHdsUQNY";
	public const string secret_id = "HkXCQy6yTjpb1MsW";

	#endregion

	private string access_token;
	public string Access_Token
	{
		get { return access_token; }
		private set { access_token = value; }
	}

	public IEnumerator GetAutoDeskToken()
	{
		#region Datos del body del webRequest

		WWWForm form = new WWWForm();
		form.AddField("client_id", cliend_id);
		form.AddField("client_secret", secret_id);
		form.AddField("grant_type", "client_credentials");
		form.AddField("scope", "data:create data:write data:read bucket:create bucket:delete bucket:read viewables:read");

		#endregion

		var www = UnityWebRequest.Post(url, form);

		www.SetRequestHeader("Content-Type", "application/x-www-form-urlencoded");

		yield return www.SendWebRequest();

		if(www.result == UnityWebRequest.Result.Success)
		{
			var parsedJSON = JSON.Parse(www.downloadHandler.text);

			if (Access_Token == null || Access_Token != parsedJSON["access_token"].Value)
				Access_Token = parsedJSON["access_token"].Value;
		}
		else
		{
			Debug.Log(www.error);
		}
	}
}
