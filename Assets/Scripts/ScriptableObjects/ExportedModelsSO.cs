using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Exported FBX Container", menuName = "Scriptable Objects/Data/Exported FBX Container")]
public class ExportedModelsSO : ScriptableObject
{

	public List<ModelExportDataHolder> models;

}
