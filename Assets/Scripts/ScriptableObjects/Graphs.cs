using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Graphs", menuName = "Scriptable Objects/Prefabs/Graphs Container")]
public class Graphs : ScriptableObject
{

	public GameObject boolean;
	public GameObject flow;
	public GameObject levels;
	public GameObject number;
	public GameObject pressure;
	public GameObject speed;
	public GameObject temperature;
	public GameObject time;

}
