﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Prefab Container", menuName = "Scriptable Objects/Prefabs/Prefab Container")]
public class Prefabs : ScriptableObject
{

	public GameObject buttonColocado;
	public GameObject buttonSinColocar;
	public GameObject buttonParent;
	public GameObject forgeInstance;
	public GameObject forgeLoadingBar;
	public GameObject dropdown;

	/*[SerializeField] GameObject reference_graph;


	public GameObject SetUpGraph(Texture2D texture)
	{
		GameObject graph = reference_graph;

		Sprite image = Sprite.Create(texture,new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));

		graph.transform.Find("GraphImage").GetComponent<Image>().sprite = image;

		return graph;
	}*/

}
