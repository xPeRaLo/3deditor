﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils.Singleton;
using TMPro;
using UnityEngine.UI;

public class UIReferences : Singleton<UIReferences>
{

	TextMeshProUGUI quantityText;
	TextMeshProUGUI quantityText2;

	GameObject Quantity;
	GameObject Quantity2;

	[SerializeField] GameObject notification_open;
	[SerializeField] GameObject notification_close;

	[Space]

	public Transform panelToSpawnModels;
	public Transform panelToSpawnKPI;
	public Transform panelToSpawnParents;
	public Transform graphContainer;
	public Transform forgeContainer;
	public Transform objectContainer;
	public Transform dropdownContainer;
	public GameObject loadingBar;
	public Button dlButton;
	public Button FPSButton;
	public Button QuitFPSButton;

	private void Awake()
	{
		Quantity = notification_open.transform.Find("Quantity").gameObject;
		quantityText = notification_open.transform.Find("Quantity").Find("Number").GetComponent<TextMeshProUGUI>();

		quantityText2 = notification_close.transform.Find("Quantity").Find("Number").GetComponent<TextMeshProUGUI>();
		Quantity2 = notification_close.transform.Find("Quantity").gameObject;
	}
	public void SetNotifications(int quantity)
	{
		quantityText.text = quantity.ToString();
		quantityText2.text = quantity.ToString();

		if (quantity > 0)
		{
			Quantity.SetActive(true);
			Quantity2.SetActive(true);
		}
	}
}
