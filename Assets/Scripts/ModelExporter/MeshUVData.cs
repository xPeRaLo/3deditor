using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

[System.Serializable]
public class MeshUVData 
{
	[SerializeField] float2[] uv, uv2, uv3, uv4, uv5, uv6, uv7, uv8;
	
	public Vector2 [] UV1 { get { return convertToVector2(uv); } } 
	public Vector2 [] UV2 { get { return convertToVector2(uv2); } } 
	public Vector2 [] UV3 { get { return convertToVector2(uv3); } } 
	public Vector2 [] UV4 { get { return convertToVector2(uv4); } } 
	public Vector2 [] UV5 { get { return convertToVector2(uv5); } } 
	public Vector2 [] UV6 { get { return convertToVector2(uv6); } } 
	public Vector2 [] UV7 { get { return convertToVector2(uv7); } } 
	public Vector2 [] UV8 { get { return convertToVector2(uv8); } } 

	public MeshUVData(Vector2 [] uv, Vector2[] uv2, Vector2[] uv3, Vector2[] uv4, Vector2[] uv5, Vector2[] uv6, Vector2[] uv7, Vector2[] uv8)
	{
		this.uv = convertToFloat2(uv);
		this.uv2 = convertToFloat2(uv2);
		this.uv3 = convertToFloat2(uv3);
		this.uv4 = convertToFloat2(uv4);
		this.uv5 = convertToFloat2(uv5);
		this.uv6 = convertToFloat2(uv6);
		this.uv7 = convertToFloat2(uv7);
		this.uv8 = convertToFloat2(uv8);
	}

	float2 [] convertToFloat2(Vector2 [] array)
	{
		float2[] conversion = new float2[array.Length];

		for(int i = 0; i < conversion.Length; i++)
		{
			conversion[i] = new float2(array[i].x, array[i].y);
		}

		return conversion;
	}

	Vector2 [] convertToVector2(float2 [] array)
	{
		Vector2[] conversion = new Vector2[array.Length];

		for (int i = 0; i < conversion.Length; i++)
		{
			conversion[i] = new Vector2(array[i].x, array[i].y);
		}

		return conversion;
	}

}
