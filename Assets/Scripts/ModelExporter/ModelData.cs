using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ModelData
{

	[SerializeField] List<MeshData> meshData = new List<MeshData>(); 
	
	public List<MeshData> MeshData { get { return meshData; } }
	public ModelData(GameObject obj)
	{
		Recursive(obj.transform);
	}

	void Recursive(Transform transform)
	{
		
		for (int i = 0; i < transform.childCount; i++)
		{

			if (transform.GetChild(i).TryGetComponent(out MeshFilter mesh) && transform.GetChild(i).TryGetComponent(out MeshRenderer renderer))
			{
				MeshData _meshData = new MeshData(mesh.mesh, renderer.materials, transform.GetChild(i).name, transform.GetChild(i));
				meshData.Add(_meshData);
			}

			Recursive(transform.GetChild(i));
		}
	}
}
