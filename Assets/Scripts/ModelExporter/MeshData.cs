using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.Mathematics;
using UnityEngine;

[Serializable]
public class MeshData 
{
	[SerializeField] string meshName;

	[SerializeField] int[] triangles;
	[SerializeField] float3[] vertices;
	[SerializeField] float3[] normals;

	[SerializeField] List<SubMeshData> subMeshes;
	[SerializeField] List<MaterialData> materialsList;

	[SerializeField] MeshUVData meshUV;

	[SerializeField] float3 position;
	[SerializeField] float4 rotation;
	[SerializeField] float3 scale;

	#region Properties
	public string MeshName { private set { meshName = value; } get { return meshName; } }
	public int [] Triangles { get { return triangles; } }
	public Vector3[] Vertices 
	{
		get
		{
			Vector3[] vertex = new Vector3[vertices.Length];

			for(int i = 0; i < vertices.Length; i++)
			{
				vertex[i] = new Vector3(vertices[i].x, vertices[i].y, vertices[i].z); 
			}

			return vertex;
		}
	}
	public Vector3[] Normals 
	{
		get
		{
			Vector3[] norm = new Vector3[normals.Length];

			for(int i = 0; i < vertices.Length; i++)
			{
				norm[i] = new Vector3(normals[i].x, normals[i].y, normals[i].z); 
			}

			return norm;
		}
	}

	public Vector3 Position
	{
		get	{ return new Vector3(position.x, position.y, position.z); }
		private set { position = value; }
	}
	public Quaternion Rotation
	{
		get	{ return new Quaternion(rotation.x, rotation.y, rotation.z, rotation.w); }
		private set { rotation = new float4(value.x, value.y, value.z, value.w); }
	}
	public Vector3 Scale
	{
		get	{ return new Vector3(scale.x, scale.y, scale.z); }
		private set { scale = value; }
	}

	public MeshUVData MeshUV 
	{ 
		get	{ return meshUV; }
		private set { meshUV = value; }
	}

	public List<SubMeshData> SubMesheses { get { return subMeshes; } }
	public List<MaterialData> MaterialsList { get { return materialsList; } }

	#endregion

	public MeshData(Mesh mesh, Material[] materials, string name, Transform transform)
	{
		MeshName = name;
		Position = transform.position;
		Rotation = transform.rotation;
		Scale = transform.lossyScale;

		triangles = mesh.triangles;
		vertices = new float3[mesh.vertices.Length];
		normals = new float3[mesh.normals.Length];

		MeshUV = new MeshUVData(mesh.uv, mesh.uv2, mesh.uv3, mesh.uv4, mesh.uv5, mesh.uv6, mesh.uv7, mesh.uv8);

		for(int i = 0; i < vertices.Length; i++)
		{
			vertices[i] = new float3(mesh.vertices[i]);
		}
		
		for(int i = 0; i < normals.Length; i++)
		{
			normals[i] = new float3(mesh.normals[i]);
		}

		if (mesh.subMeshCount > 0)
		{
			subMeshes = new List<SubMeshData>();

			for(int i = 0; i < mesh.subMeshCount; i++)
			{

				Vector3 center = mesh.GetSubMesh(i).bounds.center;
				Vector3 extents = mesh.GetSubMesh(i).bounds.extents;
				Vector3 max = mesh.GetSubMesh(i).bounds.max;
				Vector3 min = mesh.GetSubMesh(i).bounds.min;
				Vector3 size = mesh.GetSubMesh(i).bounds.size;
				SubMeshData.SubMeshBounds bounds = new SubMeshData.SubMeshBounds(center, extents, max, min, size);
;
				int baseVertex = mesh.GetSubMesh(i).baseVertex;
				int firstVertex = mesh.GetSubMesh(i).firstVertex;
				int indexCount = mesh.GetSubMesh(i).indexCount;
				int indexStart = mesh.GetSubMesh(i).indexStart;
				MeshTopology topology = mesh.GetSubMesh(i).topology;
				int vertexCount = mesh.GetSubMesh(i).vertexCount;

				SubMeshData subMesh = new SubMeshData(baseVertex, bounds, firstVertex, indexCount, indexStart, topology, vertexCount, name);

				subMeshes.Add(subMesh);
			}
		}

		if(materials.Length > 0)
		{
			materialsList = new List<MaterialData>();

			for(int i = 0; i < materials.Length; i++)
			{
				List<TextureAtt> textures = new List<TextureAtt>();
				List<PropertyAtt> properties = new List<PropertyAtt>();
				List<ColorAtt> colors = new List<ColorAtt>();

				MaterialData mData = new MaterialData(materials[i], textures, properties, colors);
				materialsList.Add(mData);
			}
		}
	}

}
