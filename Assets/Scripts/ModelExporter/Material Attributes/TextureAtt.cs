using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum TextureType
{
	_MainTex,
	_MetallicGlossMap,
	_BumpMap,
	_ParallaxMap,
	_OcclusionMap,
	_EmissionMap,
	_DetailMask,
	_DetailAlbedoMap,
	_DetailNormalMap
}

[System.Serializable]
public class TextureAtt
{

	[SerializeField] TextureType textureType;
	[SerializeField] byte[] texture;

	[SerializeField] int width, height;

	public TextureType _TextureType { get { return textureType; } }
	public byte [] Texture { get { return texture; } }
	
	public int Width { get { return width; } }
	public int Height { get { return height; } }

	public TextureAtt(TextureType textureType, byte [] texture, int width, int height)
	{
		this.textureType = textureType;
		this.texture = texture;
		this.width = width;
		this.height = height;
	}

}
