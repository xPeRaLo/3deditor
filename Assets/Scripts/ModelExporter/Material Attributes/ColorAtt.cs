using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ColorAttribute
{
	_Color,
	_EmissionColor
}

[System.Serializable]
public class ColorAtt 
{
	[SerializeField] ColorAttribute colorAttribute;
	[SerializeField] Color color;

	public ColorAttribute _ColorAttribute { get { return colorAttribute; } }
	public Color Color { get { return color; } }
	public ColorAtt(ColorAttribute colorAttribute, Color color)
	{
		this.colorAttribute = colorAttribute;
		this.color = color;
	}
}
