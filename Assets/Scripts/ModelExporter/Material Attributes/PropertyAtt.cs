using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MaterialAttribute
{
	_Cutoff,
	_Glossiness,
	_GlossMapScale,
	_SmoothnessTextureChannel,
	_Metallic,
	_SpecularHighlights,
	_GlossyReflections,
	_BumpScale,
	_Parallax,
	_OcclusionStrength,
	_DetailNormalMapScale,
	_UVSec,
	_Mode
}

[System.Serializable]
public class PropertyAtt
{
	[SerializeField] MaterialAttribute materialAttribute;
	[SerializeField] float value;

	public MaterialAttribute _MaterialAttribute { get { return materialAttribute; } }
	public float Value { get { return value; } }

	public PropertyAtt(MaterialAttribute materialAttribute, float value)
	{
		this.materialAttribute = materialAttribute;
		this.value = value;
	}
}
