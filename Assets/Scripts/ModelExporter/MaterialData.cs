using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MaterialData
{

	[SerializeField] List<TextureAtt> textures = new List<TextureAtt>();
	[SerializeField] List<PropertyAtt> properties = new List<PropertyAtt>();
	[SerializeField] List<ColorAtt> colors = new List<ColorAtt>();

	public List<TextureAtt> Textures { private set { textures = value; } get { return textures; } }
	public List<PropertyAtt> Properties { private set { properties = value; } get { return properties; } }
	public List<ColorAtt> Colors { private set { colors = value; } get { return colors; } }

	public MaterialData(Material material, List<TextureAtt> textures, List<PropertyAtt> properties, List<ColorAtt> colors)
	{
		MaterialAttributes.GetMaterial(material, out textures, out properties, out colors);

		Textures = textures;
		Properties = properties;
		Colors = colors;
	}

	
}
