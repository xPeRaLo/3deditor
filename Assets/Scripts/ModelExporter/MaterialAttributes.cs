using System.Collections;
using System.Collections.Generic;
using TriLibCore.General;
using UnityEditor;
using UnityEngine;

[System.Serializable]
public static class MaterialAttributes
{

	public static void GetMaterial(Material material, out List<TextureAtt> textures, out List<PropertyAtt> properties, out List<ColorAtt> colors)
	{
		textures = GetMaterialTextures(material);
		properties = GetMaterialProperties(material);
		colors = GetMaterialColors(material);
	}

	public static Material SetMaterial(List<TextureAtt> textures, List<PropertyAtt> properties, List<ColorAtt> colors)
	{
		Material materialOutPut = new Material(Shader.Find("Standard"));
		materialOutPut.name = "Imported";
		materialOutPut.renderQueue = 3000;

		materialOutPut.enableInstancing = false;

		foreach (TextureAtt texture in textures) // Texturas
		{
			Texture2D tx = new Texture2D(texture.Width, texture.Height, UnityEngine.TextureFormat.RGBA32, false);
			tx.LoadImage(texture.Texture);
			tx.Apply();

			materialOutPut.SetTexture(texture._TextureType.ToString(), tx);
		}

		foreach (PropertyAtt property in properties) // propiedades de materiales
		{
			materialOutPut.SetFloat(property._MaterialAttribute.ToString(), property.Value);
		}

		foreach (ColorAtt color in colors) // Colores
		{
			materialOutPut.SetColor(color._ColorAttribute.ToString(), color.Color);
		}
		return materialOutPut;
	}
	static List<TextureAtt> GetMaterialTextures(Material material)
	{

		List<TextureAtt> textures = new List<TextureAtt>();

		#region Texturas

		Texture2D mainTexture = (Texture2D) material.GetTexture(TextureType._MainTex.ToString());
		Texture2D metallicGlossMap = (Texture2D) material.GetTexture(TextureType._MetallicGlossMap.ToString());
		Texture2D bumpMap = (Texture2D) material.GetTexture(TextureType._BumpMap.ToString());
		Texture2D parallaxMap = (Texture2D) material.GetTexture(TextureType._ParallaxMap.ToString());
		Texture2D occlusionMap = (Texture2D) material.GetTexture(TextureType._OcclusionMap.ToString());
		Texture2D emissionMap = (Texture2D) material.GetTexture(TextureType._EmissionMap.ToString());
		Texture2D detailMask = (Texture2D) material.GetTexture(TextureType._DetailMask.ToString());
		Texture2D detailAlbedoMap = (Texture2D) material.GetTexture(TextureType._DetailAlbedoMap.ToString());
		Texture2D detailNormalMap = (Texture2D) material.GetTexture(TextureType._DetailNormalMap.ToString());	

		#endregion

		// Llenamos el diccionario
		
		if (mainTexture != null) textures.Add(new TextureAtt(TextureType._MainTex, mainTexture.EncodeToPNG(),mainTexture.width, mainTexture.height));
		if (metallicGlossMap != null) textures.Add(new TextureAtt(TextureType._MetallicGlossMap, metallicGlossMap.EncodeToPNG(), mainTexture.width, mainTexture.height));
		if (bumpMap != null) textures.Add(new TextureAtt(TextureType._BumpMap, bumpMap.EncodeToPNG(), mainTexture.width, mainTexture.height));
		if (parallaxMap != null) textures.Add(new TextureAtt(TextureType._ParallaxMap, parallaxMap.EncodeToPNG(), mainTexture.width, mainTexture.height));
		if (occlusionMap != null) textures.Add(new TextureAtt(TextureType._OcclusionMap, occlusionMap.EncodeToPNG(), mainTexture.width, mainTexture.height));
		if (emissionMap != null) textures.Add(new TextureAtt(TextureType._EmissionMap, emissionMap.EncodeToPNG(), mainTexture.width, mainTexture.height));
		if (detailMask != null) textures.Add(new TextureAtt(TextureType._DetailMask, detailMask.EncodeToPNG(), mainTexture.width, mainTexture.height));
		if (detailAlbedoMap != null) textures.Add(new TextureAtt(TextureType._DetailAlbedoMap, detailAlbedoMap.EncodeToPNG(), mainTexture.width, mainTexture.height));
		if (detailNormalMap != null) textures.Add(new TextureAtt(TextureType._DetailNormalMap, detailNormalMap.EncodeToPNG(), mainTexture.width, mainTexture.height));

		return textures;
	}

	static List<PropertyAtt> GetMaterialProperties(Material material)
	{
		List<PropertyAtt> properties = new List<PropertyAtt>();

		#region Propiedades 

		float cutoff = material.GetFloat(MaterialAttribute._Cutoff.ToString());
		float glossiness = material.GetFloat(MaterialAttribute._Glossiness.ToString());
		float glossMapScale = material.GetFloat(MaterialAttribute._GlossMapScale.ToString());
		float smoothnessTextureChannel = material.GetFloat(MaterialAttribute._SmoothnessTextureChannel.ToString());
		float metallic = material.GetFloat(MaterialAttribute._Metallic.ToString());
		float specullarHighlights = material.GetFloat(MaterialAttribute._SpecularHighlights.ToString());
		float glossyReflections = material.GetFloat(MaterialAttribute._GlossyReflections.ToString());
		float bumpScale = material.GetFloat(MaterialAttribute._BumpScale.ToString());
		float parallax = material.GetFloat(MaterialAttribute._Parallax.ToString());
		float occlusionStrength = material.GetFloat(MaterialAttribute._OcclusionStrength.ToString());
		float detailNormalMapScale = material.GetFloat(MaterialAttribute._DetailNormalMapScale.ToString());
		float uvSec = material.GetFloat(MaterialAttribute._UVSec.ToString());
		float mode = material.GetFloat(MaterialAttribute._Mode.ToString());


		#endregion

		properties.Add(new PropertyAtt(MaterialAttribute._Cutoff, cutoff));
		properties.Add(new PropertyAtt(MaterialAttribute._Glossiness, glossiness));
		properties.Add(new PropertyAtt(MaterialAttribute._GlossMapScale, glossMapScale));
		properties.Add(new PropertyAtt(MaterialAttribute._SmoothnessTextureChannel, smoothnessTextureChannel));
		properties.Add(new PropertyAtt(MaterialAttribute._Metallic, metallic));
		properties.Add(new PropertyAtt(MaterialAttribute._SpecularHighlights, specullarHighlights));
		properties.Add(new PropertyAtt(MaterialAttribute._GlossyReflections, glossyReflections));
		properties.Add(new PropertyAtt(MaterialAttribute._BumpScale, bumpScale));
		properties.Add(new PropertyAtt(MaterialAttribute._Parallax, parallax));
		properties.Add(new PropertyAtt(MaterialAttribute._OcclusionStrength, occlusionStrength));
		properties.Add(new PropertyAtt(MaterialAttribute._DetailNormalMapScale, detailNormalMapScale));
		properties.Add(new PropertyAtt(MaterialAttribute._UVSec, uvSec));
		properties.Add(new PropertyAtt(MaterialAttribute._Mode, mode));
				
		return properties;
	}

	static List<ColorAtt> GetMaterialColors(Material material)
	{
		List<ColorAtt> colors = new List<ColorAtt>();

		#region Colores

		Color color = material.GetColor(ColorAttribute._Color.ToString());
		Color emissionColor = material.GetColor(ColorAttribute._EmissionColor.ToString());

		#endregion

		colors.Add(new ColorAtt(ColorAttribute._Color, color));
		colors.Add(new ColorAtt(ColorAttribute._EmissionColor, emissionColor));

		return colors;
	}

}
