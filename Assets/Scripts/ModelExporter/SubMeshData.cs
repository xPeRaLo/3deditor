using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

[System.Serializable]
public class SubMeshData 
{

	#region Properties 

	[SerializeField] int baseVertex;
	[SerializeField] SubMeshBounds bounds;
	[SerializeField] int firstVertex;
	[SerializeField] int indexCount;
	[SerializeField] int indexStart;
	[SerializeField] MeshTopology topology;
	[SerializeField] int vertexCount;
	[SerializeField] string subMeshName;
	public int BaseVertex { private set { baseVertex = value; } get { return baseVertex; } }
	public SubMeshBounds Bounds { private set { bounds = value; } get { return bounds; } }
	public int FirstVertex { private set { firstVertex = value; } get { return firstVertex; } }
	public int IndexCount { private set { indexCount = value; } get { return indexCount; } }
	public int IndexStart { private set { indexStart = value; } get { return indexStart; } }
	public MeshTopology Topology { private set { topology = value; } get { return topology; } }
	public int VertexCount { private set { vertexCount = value; } get { return vertexCount; } }
	public string SubMeshName { private set { subMeshName = value; } get { return subMeshName; } }
	#endregion

	public SubMeshData(int baseVertex,SubMeshBounds bounds, int firstVertex , int indexCount, int indexStart, MeshTopology topology, int vertexCount, string name)
	{
		BaseVertex = baseVertex;
		Bounds = bounds;
		FirstVertex = firstVertex;
		IndexCount = indexCount;
		IndexStart = indexStart;
		Topology = topology;
		VertexCount = vertexCount;
		SubMeshName = name;
	}
	
	[System.Serializable]
	public class SubMeshBounds
	{

		#region Properties

		[SerializeField] float3 center;
		[SerializeField] float3 extents;
		[SerializeField] float3 max;
		[SerializeField] float3 min;
		[SerializeField] float3 size;

		public float3 Center { private set { center = value; } get { return new Vector3(center.x, center.y, center.z); } }
		public float3 Extents { private set { extents = value; } get { return new Vector3(extents.x, extents.y, extents.z); } }
		public float3 Max { private set { max = value; } get { return new Vector3(max.x, max.y, max.z); } }
		public float3 Min { private set { min = value; } get { return new Vector3(min.x, min.y, min.z); } }
		public float3 Size { private set { size = value; } get { return new Vector3(size.x, size.y, size.z); } }

		#endregion

		public SubMeshBounds(Vector3 center, Vector3 extents, Vector3 max, Vector3 min, Vector3 size)
		{
			Center = new float3(center.x, center.y, center.z);
			Extents = new float3(extents.x, extents.y, extents.z);
			Max = new float3(max.x, max.y, max.z);
			Min = new float3(min.x, min.y, min.z);
			Size = new float3(size.x, size.y, size.z);
		}

	}

}
