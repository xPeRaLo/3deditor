﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DataForExport
{
	public ModelProperty model3d;
	public KPIExport[] KPIs;

	public DataForExport(ModelProperty modelProperties, KPIExport[] kpis)
	{
		model3d = modelProperties;
		KPIs = kpis;
	}
}

