﻿using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

[System.Serializable]
public class ModelProperty
{
	public float [] position;
	public float [] rotation;
	public float [] scale;


	public ModelProperty(Vector3? position, Vector3? rotation, Vector3? scale)
	{
		if(position.HasValue && rotation.HasValue && scale.HasValue)
		{
			this.position = new float[] { position.Value.x, position.Value.y, position.Value.z };
			this.rotation = new float[] { rotation.Value.x, rotation.Value.y, rotation.Value.z };
			this.scale = new float[] { scale.Value.x, scale.Value.y, scale.Value.z };
		}
	}
}

