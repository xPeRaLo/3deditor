﻿using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using System;

[Serializable]
public class KPIExport
{
	public int kpi;

	public float[] position;
	public float[] rotation;
	public float[] scale;

	public bool panel;

	public bool set;

	public KPIExport(int id, bool panel = false)
	{
		kpi = id;
		this.panel = panel;
	}

	public void Set(Vector3? position, Vector3? rotation, Vector3? scale)
	{
		this.position = new float[] { position.Value.x, position.Value.y, position.Value.z };
		this.rotation = new float[] { rotation.Value.x, rotation.Value.y, rotation.Value.z };
		this.scale = new float[] { scale.Value.x, scale.Value.y, scale.Value.z };

		set = true;
	}
}
