﻿using UnityEngine;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;
using UnityEditor;

/// <summary>
/// Libreria de utlidades, Editada por ultima por Ander Peralo el 15/07/2021 || 11:20
/// </summary>

namespace Utils
{

	namespace FileSystem
	{
		public static class FileSystem
		{

			#region Directorios

			static string modelDir = Application.persistentDataPath+"/Model Information/"; // Directorio por defecto donde exportar
			static string kpiDir = Application.persistentDataPath + "/KPI Information/";
			static string FBXDir = Application.persistentDataPath + "/FBX/";

			public static string models = modelDir;
			public static string kpis = kpiDir;
			public static string fbx = FBXDir;

			#endregion

			#region Exportar

			#region Exportar por defecto

			public static void ExportData<Data>(Data data,string fileName = "export", string extension = "imm")
			{
						
				Directory.CreateDirectory(models);

				string destination = models + fileName + "." + extension;
				FileStream file;

				file = File.Create(destination);

				BinaryFormatter bf = new BinaryFormatter();
				bf.Serialize(file, data);

				file.Close();
			}

			#endregion

				#region Exportar a una carpeta especifica

			/// <summary>
			/// Exports the data to a custom data path inside Application.PersistentDataPath
			/// </summary>
			/// <typeparam name="Data">Object to Export</typeparam>
			/// <param name="parentFolder">The name of the parent folder inside persistent Data path</param>
			/// <param name="data">Object to export</param>
			/// <param name="folderName">Name of the folder which the file will be placed</param>
			/// <param name="fileName">Name of the file</param>
			/// <param name="extension">Extension of the file</param>
			public static void ExportDataToCustomFolder<Data>(string parentFolder, Data data, string folderName, string fileName = "export", string extension = "imm")
			{

				try
				{
					DeleteFolder(parentFolder+folderName); // La elimina para sobreescribir

					Directory.CreateDirectory(parentFolder + "/" + folderName);

					string destination = parentFolder + folderName + "/" + fileName + "." + extension;
					FileStream file;

					file = File.Create(destination);
					BinaryFormatter bf = new BinaryFormatter();
					bf.Serialize(file, data);

					file.Close();
				}
				catch (Exception e)
				{
					Debug.Log("ERROR: " + e.ToString()); // TODO: Mostrar ventana de error.
				}
			}	

			#endregion

			#endregion

			#region Importar

				#region Importar por defecto

					public static Data ImportData<Data>(string fileName = "export", string extension = "m1")
					{
						string destination = modelDir + fileName + "." + extension; ;
						FileStream file;

						if (File.Exists(destination))
							file = File.OpenRead(destination);
						else
							return default;

						BinaryFormatter bf = new BinaryFormatter();
						Data data = (Data)bf.Deserialize(file);

						file.Close();

						return data;
					}

			#endregion

				#region Importar de una carpeta específica

					/// <summary>
					/// 
					/// </summary>
					/// <typeparam name="Data">Custom type</typeparam>
					/// <param name="parent">Name of the parent folder which all the data may be stored</param>
					/// <param name="folderName">Name of the folder that contains this file (Usually named equal to the file name)</param>
					/// <param name="fileName">Name of the file</param>
					/// <param name="extension">Extension of the file</param>
					/// <returns></returns>
					public static Data ImportDataFromCustomFolder<Data>(string parent,string folderName,string fileName = "export", string extension = "imm")
					{


						string destination = parent + folderName + "/" + fileName + "." + extension;
						FileStream file;

							if (SearchFolder(parent+folderName)) 
							{

								if (File.Exists(destination))
									file = File.OpenRead(destination);
								else
									return default;


								BinaryFormatter bf = new BinaryFormatter();
								Data data = (Data)bf.Deserialize(file);


								file.Close();

								return data;
							}

							else
							{
								Debug.LogError("Path: " + destination + " does not exist");
								return default;
							}
					}

			#endregion

			#endregion

			#region Manejo de datos

				#region Eliminacion de datos de usuario


				public static void DeleteUserData()
				{
					string destination = modelDir + "/export.m1";

					if (File.Exists(destination))
					File.Delete(destination);
				}

				#endregion

				#region Manejo de carpetas

					#region Creacion de carpetas

						public static void CreateFolder(string folderName)
						{
						string directory = models + folderName;

						if (!Directory.Exists(directory))
							Directory.CreateDirectory(directory);
						}

					#endregion

					#region Eliminación de carpetas

						public static void DeleteFolder(string folderName)
						{
							string directory = models + folderName;

							if (Directory.Exists(directory))
								Directory.Delete(directory, true);
			
							
						}

			#endregion

			#endregion

			#endregion

			#region Obtencion de datos

				#region Cantidad de ficheros en una carpeta

					public static int FileNumber(string folderName = "")
					{
						string directory = modelDir + "/"+folderName;

						if (SearchFolder(folderName))
							return Directory.GetFiles(directory).Length;
						
						else
							return 0;
					}

				#endregion

				#region Cantidad de carpetas dentro de una carpeta

					public static int FolderAmount(string parent,string folderName = null)
					{
						string directory = modelDir + folderName;

						if (SearchFolder(parent+folderName))
						{
							DirectoryInfo info = new DirectoryInfo(directory);
							return info.GetDirectories().Length;
						}

						return 0;
					}

				#endregion

				#region Busqueda de carpetas

					public static bool SearchFolder(string parentFolder,string folderName = null)
					{
						string directory = parentFolder + folderName;

						if (Directory.Exists(directory))
							return true;

						return false;
					}

				#endregion

				#region Obtencion de todos los modelos disponibles

					public static List<string> GetFolderNames(string parent)
					{

						DirectoryInfo info = new DirectoryInfo(parent);
						DirectoryInfo[] directories = info.GetDirectories();

						List<string> folderNames = new List<string>();

						foreach(DirectoryInfo dir in directories)
						{
							folderNames.Add(dir.Name);
						}
						
						return folderNames;
					}

			#endregion

				#region Busqueda de carpeta

				public static bool folderExisting(string parent,string folderName)
				{

					List<string> folderNames = GetFolderNames(parent);

					foreach(string s in folderNames)
					{

						if (s.ToLower() == folderName.ToLower())
							return true;
					}

					return false;
				}

				#endregion

			#endregion
		}
	}

	namespace Singleton
	{
		public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
		{
			// Check to see if we're about to be destroyed.
			private static bool m_ShuttingDown = false;
			private static object m_Lock = new object();
			private static T m_Instance;

			/// <summary>
			/// Access singleton instance through this propriety.
			/// </summary>
			public static T Instance
			{
				get
				{
					if (m_ShuttingDown)
					{
						Debug.LogWarning("[Singleton] Instance '" + typeof(T) +
							"' already destroyed. Returning null.");
						return null;
					}

					lock (m_Lock)
					{
						if (m_Instance == null)
						{
							// Search for existing instance.
							m_Instance = (T)FindObjectOfType(typeof(T));

							// Create new instance if one doesn't already exist.
							if (m_Instance == null)
							{
								// Need to create a new GameObject to attach the singleton to.
								var singletonObject = new GameObject();
								m_Instance = singletonObject.AddComponent<T>();
								singletonObject.name = typeof(T).ToString() + " (Singleton)";

								// Make instance persistent.
								DontDestroyOnLoad(singletonObject);
							}
						}

						return m_Instance;
					}
				}
			}


			private void OnApplicationQuit()
			{
				m_ShuttingDown = true;
			}


			private void OnDestroy()
			{
				m_ShuttingDown = true;
			}
		}
	}

	namespace Math
	{
		public static class MathFuncs
		{
			public static float NormalizedValue(float CurrentValue,float minValue, float MaxValue)
			{
				return (CurrentValue - minValue) / (MaxValue - minValue);
			}
		}
		
	}

	namespace Exporter
	{
		public static class FBXUtils
		{
			public static void ExportGameObject(GameObject objectToExport)
			{
				

				foreach(Transform g in objectToExport.transform)
				{

					File.Create(Application.persistentDataPath + $"/ModelExport/{objectToExport.name} - {g.name}.json");

					ModelData model = new ModelData(g.gameObject);
					string json = JsonUtility.ToJson(model);
					File.WriteAllText(Application.persistentDataPath + $"/ModelExport/{objectToExport.name} - {g.name}.json", json);
				}

				

				//FileSystem.FileSystem.ExportDataToCustomFolder<ModelData>("Model properties", model, objectToExport.name, objectToExport.name, "json");

			}

			public static List<ModelData> ImportGameObject(string objectName)
			{
				List<ModelData> models = new List<ModelData>();

				DirectoryInfo d = new DirectoryInfo(Application.persistentDataPath+"/ModelExport/");

				FileInfo[] Files = d.GetFiles($"{objectName} - *.json"); //Getting Text files
				
				foreach(FileInfo file in Files)
				{
					FileStream stream = file.OpenRead();
					StreamReader streamReader = file.OpenText();
					
					string json = streamReader.ReadToEnd();

					ModelData data = JsonUtility.FromJson<ModelData>(json);

					models.Add(data);
				}

				return models;
				

				//return JsonUtility.FromJson<ModelData>(json);
			}
		}
	
	}
}



