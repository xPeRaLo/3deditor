
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;


namespace UnityStandardAssets.Characters.FirstPerson
{
    [RequireComponent(typeof (CharacterController))]
    public class FirstPersonController : MonoBehaviour
    {
        [SerializeField] private float m_WalkSpeed;
        [SerializeField] [Range(0f, 1f)] private float m_RunstepLenghten;
        [SerializeField] private MouseLook m_MouseLook;
        [SerializeField] private float m_StepInterval;
        private Camera m_Camera;
        private Camera UICamera;
        private Vector2 m_Input;
        private Vector3 m_MoveDir = Vector3.zero;
        private CharacterController m_CharacterController;
        private CollisionFlags m_CollisionFlags;
        private float m_StepCycle;
        private float m_NextStep;
        // Use this for initialization
        private void Start()
        {
            m_CharacterController = GetComponent<CharacterController>();
            m_Camera = Camera.main;
            UICamera = transform.GetChild(1).GetComponent<Camera>();
            m_StepCycle = 0f;
            m_NextStep = m_StepCycle/2f;
			m_MouseLook.Init(transform , m_Camera.transform, UICamera.transform);
        }


        // Update is called once per frame
        private void Update()
        {
            RotateView();
        }

        private void FixedUpdate()
        {
            float speed;
            GetInput(out speed);
            
            float y = 0;

            if (Input.GetButton("Jump"))
                y += 1;

            if (Input.GetButton("Cam_Down"))
                y -= 1;           

            Vector3 desiredMove = transform.forward * m_Input.y + transform.right * m_Input.x;

            m_MoveDir.x = desiredMove.x*speed;
            m_MoveDir.y = y * (speed);
            m_MoveDir.z = desiredMove.z*speed;
         
            m_CollisionFlags = m_CharacterController.Move(m_MoveDir*Time.fixedDeltaTime);

            ProgressStepCycle(speed);
        }

        private void ProgressStepCycle(float speed)
        {
            if (m_CharacterController.velocity.sqrMagnitude > 0 && (m_Input.x != 0 || m_Input.y != 0))
            {
                m_StepCycle += (m_CharacterController.velocity.magnitude + (speed* m_WalkSpeed))*
                             Time.fixedDeltaTime;
            }

            if (!(m_StepCycle > m_NextStep))
            {
                return;
            }

            m_NextStep = m_StepCycle + m_StepInterval;
        }

        private void GetInput(out float speed)
        {
            // Read input
            float horizontal = CrossPlatformInputManager.GetAxis("Horizontal");
            float vertical = CrossPlatformInputManager.GetAxis("Vertical");

            speed = m_WalkSpeed;
            m_Input = new Vector2(horizontal,vertical);

            // normalize input if it exceeds 1 in combined length:
            if (m_Input.sqrMagnitude > 1)
            {
                m_Input.Normalize();
            }
        }

        private void RotateView()
        {
            m_MouseLook.LookRotation (transform, m_Camera.transform, UICamera.transform);
        }
    }
}
